﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using JODataLibrary.JODataManagers;
using JODataLibrary.JODataObjects;

namespace JODataLibrary
{
    /// <summary>
    /// The entry point to all music data.
    /// </summary>
    public class JODataProvider
    {
        private readonly string _databaseFilename;

        #region Managers
        private List<JODataManagerBaseClass> AllManagers { get; }

        private JOInstrumentManager _instrumentManager { get; }
        /// <summary>
        /// The list of instruments.
        /// </summary>
        public IReadOnlyCollection<JOInstrument> Instruments => _instrumentManager.Instruments;

        private JOPlayerManager _playerManager { get; }
        /// <summary>
        /// The list of players.
        /// </summary>
        public IReadOnlyCollection<JOPlayer> Players => _playerManager.Players;

        private JOProgramManager _programManager { get; }
        /// <summary>
        /// The list of programs.
        /// </summary>
        public IReadOnlyCollection<JOProgram> Programs => _programManager.Programs;

        private JOPieceManager _pieceManager { get; }
        /// <summary>
        /// The list of pieces.
        /// </summary>
        public IReadOnlyCollection<JOPiece> Pieces => _pieceManager.Pieces;
        #endregion

        public JODataProvider( string databaseFilename )
        {
            _databaseFilename = databaseFilename;

            /** Create the managers.  The order of creation is important here because the Load() function will
             *  give each manager a chancen to load in order, and the managers have the following dependencies:
             *  Players have a depenency on Instruments
             *  Pieces have a dependency on Instruments
             *  Programs have a dependency on Pieces
             */
            AllManagers = new List<JODataManagerBaseClass>();
            _instrumentManager = new JOInstrumentManager( this );
            AllManagers.Add( _instrumentManager );
            _playerManager = new JOPlayerManager( this );
            AllManagers.Add( _playerManager );
            _pieceManager = new JOPieceManager( this );
            AllManagers.Add( _pieceManager );
            _programManager = new JOProgramManager( this );
            AllManagers.Add( _programManager );
            
            Load();
        }

        /// <summary>
        /// Adds an instrument.
        /// </summary>
        /// <param name="instrument">The instrument to add.</param>
        public void AddInstrument( JOInstrument instrument )
        {
            _instrumentManager.AddInstrument( instrument );
        }

        /// <summary>
        /// Removes an instrument.
        /// </summary>
        /// <param name="instrument">The instrument to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        public bool RemoveInstrument( JOInstrument instrument )
        {
            return _instrumentManager.RemoveInstrument( instrument );
        }

        /// <summary>
        /// Adds a player.
        /// </summary>
        /// <param name="Player">The player to add.</param>
        public void AddPlayer( JOPlayer Player )
        {
            _playerManager.AddPlayer( Player );
        }

        /// <summary>
        /// Removes a player.
        /// </summary>
        /// <param name="player">The player to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        public bool RemovePlayer( JOPlayer player )
        {
            return _playerManager.RemovePlayer( player );
        }

        /// <summary>
        /// Adds a program.
        /// </summary>
        /// <param name="program">The program to add.</param>
        public void AddProgram( JOProgram program )
        {
            _programManager.AddProgram( program );
        }

        /// <summary>
        /// Removes a program.
        /// </summary>
        /// <param name="program">The program to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        public bool RemoveProgram( JOProgram program )
        {
            return _programManager.RemoveProgram( program );
        }

        /// <summary>
        /// Adds a piece.
        /// </summary>
        /// <param name="piece">The piece to add.</param>
        public void AddPiece( JOPiece piece )
        {
            _pieceManager.AddPiece( piece );
        }

        /// <summary>
        /// Removes a piece.
        /// </summary>
        /// <param name="piece">The piece to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        public bool RemovePiece( JOPiece piece )
        {
            return _pieceManager.RemovePiece( piece );
        }
        
        #region SQLite
        private void Load()
        {
            using( SQLiteConnection fileConn = new SQLiteConnection( $"DataSource={_databaseFilename};Version=3" ) )
            {
                fileConn.Open();

                // Have each manager load their data from the database
                foreach( JODataManagerBaseClass manager in AllManagers )
                {
                    manager.LoadFromDatabase( fileConn );
                }
            }
        }

        public void Save()
        {
            // Open the connection to the in-memory database
            using( SQLiteConnection memConn = new SQLiteConnection( "DataSource=:memory:;Version=3" ) )
            {
                memConn.Open();

                // Have each manager save out their data to the database
                foreach( JODataManagerBaseClass manager in AllManagers )
                {
                    manager.SaveToDatabase( memConn );
                }

                // Backup the in-memory database to the file database
                using( SQLiteConnection fileConn = new SQLiteConnection( $"DataSource={_databaseFilename};Version=3" ) )
                {
                    fileConn.Open();
                    memConn.BackupDatabase( fileConn, "main", "main", -1, null, 0 );
                }
            }
        }
        #endregion

        public override string ToString()
        {
            string str = string.Empty;
            foreach( JODataManagerBaseClass manager in AllManagers )
            {
                str += $"{manager}{Environment.NewLine}";
            }

            return str;
        }
    }
}
