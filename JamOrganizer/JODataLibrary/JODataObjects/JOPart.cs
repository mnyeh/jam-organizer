﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding a part for a piece.
    /// </summary>
    public class JOPart : JODataObjectBaseClass
    {
        /// <summary>
        /// The filename of the part.
        /// </summary>
        virtual public string Filename { get; set; }

        /// <summary>
        /// The instrument that this part is for.
        /// </summary>
        virtual public JOInstrument Instrument { get; set; }

        /// <summary>
        /// Whether or not the part is an alternate part.
        /// </summary>
        virtual public bool IsAlternatePart { get; set; }

        /// <summary>
        /// Notes regarding this part.
        /// </summary>
        virtual public string Notes { get; set; }

        private readonly List<JOPart> _alternateParts = new List<JOPart>();
        /// <summary>
        /// The list of alternate parts.
        /// </summary>
        virtual public IReadOnlyCollection<JOPart> AlternateParts => _alternateParts;

        public JOPart( Guid guid, string filename, JOInstrument instrument, bool isAlternatePart = false, string notes = "" ) : base( guid )
        {
            Filename = filename;
            Instrument = instrument;
            IsAlternatePart = isAlternatePart;
            Notes = notes;
        }

        protected JOPart() : base( Guid.Empty )
        {
        }

        /// <summary>
        /// Adds an alternate part to this part.
        /// </summary>
        /// <param name="alternatePart">The alternate part to add.</param>
        virtual public void AddAlternatePart( JOPart alternatePart )
        {
            _alternateParts.Add( alternatePart );
        }

        /// <summary>
        /// Removes an alternate part from this part.
        /// </summary>
        /// <param name="alternatePart">The alternate part to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        virtual public bool RemoveAlternatePart( JOPart alternatePart )
        {
            return _alternateParts.Remove( alternatePart );
        }
    }
}
