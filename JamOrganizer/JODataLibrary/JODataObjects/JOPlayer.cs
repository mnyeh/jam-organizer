﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding a player.
    /// </summary>
    public class JOPlayer : JODataObjectBaseClass
    {
        /// <summary>
        /// The full name of the player.
        /// </summary>
        virtual public string Name { get; set; }

        /// <summary>
        /// The primary instrument of the player.
        /// </summary>
        virtual public JOInstrument PrimaryInstrument { get; set; }

        /// <summary>
        /// The email address of the player.
        /// </summary>
        virtual public string Email { get; set; }

        /// <summary>
        /// Notes regarding the player.
        /// </summary>
        virtual public string Notes { get; set; }
        
        public JOPlayer( Guid guid, string name, JOInstrument primaryInstrument, string email, string notes = "" ) : base( guid )
        {
            Name = name;
            PrimaryInstrument = primaryInstrument;
            Email = email;
            Notes = notes;
        }

        protected JOPlayer() : base( Guid.Empty )
        {
        }
    }
}
