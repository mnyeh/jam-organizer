﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding a program.
    /// </summary>
    public class JOProgram : JODataObjectBaseClass
    {
        /// <summary>
        /// The name of the program.
        /// </summary>
        virtual public string Name { get; set; }

        /// <summary>
        /// Whether or not the program is active.
        /// </summary>
        virtual public bool Active { get; set; }

        /// <summary>
        /// Notes regarding the program.
        /// </summary>
        virtual public string Notes { get; set; }

        private readonly List<JOPiece> _pieces = new List<JOPiece>();
        /// <summary>
        /// The list of pieces for the program.
        /// </summary>
        virtual public IReadOnlyCollection<JOPiece> Pieces => _pieces;

        private readonly Dictionary<JOPlayer, JOInstrument> _playersToInstruments = new Dictionary<JOPlayer, JOInstrument>();
        /// <summary>
        /// The list of players for the program.
        /// </summary>
        virtual public IReadOnlyCollection<JOPlayer> Players => _playersToInstruments.Keys;

        public JOProgram( Guid guid, string name, bool active = true, string notes = "" ) : base( guid )
        {
            Name = name;
            Active = active;
            Notes = notes;
        }

        protected JOProgram() : base( Guid.Empty )
        {
        }

        /// <summary>
        /// Returns the instrument that a player is playing for the program.
        /// </summary>
        /// <param name="player">The player to get the instrument for.</param>
        /// <returns>The instrument that a player is playing in the program.</returns>
        virtual public JOInstrument GetInstrumentForPlayer( JOPlayer player )
        {
            return _playersToInstruments[ player ];
        }

        /// <summary>
        /// Sets the instrument that the player is playing for the program.
        /// </summary>
        /// <param name="instrument"></param>
        /// <param name="player"></param>
        virtual public void SetInstrumentForPlayer( JOInstrument instrument, JOPlayer player )
        {
            _playersToInstruments[ player ] = instrument;
        }

        /// <summary>
        /// Adds a piece to the program.
        /// </summary>
        /// <param name="piece">The piece to add.</param>
        virtual public void AddPiece( JOPiece piece )
        {
            _pieces.Add( piece );
        }

        /// <summary>
        /// Removes a piece from the program.
        /// </summary>
        /// <param name="piece">The piece to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        virtual public bool RemovePiece( JOPiece piece )
        {
            return _pieces.Remove( piece );
        }

        /// <summary>
        /// Adds a player to the program.
        /// </summary>
        /// <param name="player">The player to add.</param>
        virtual public void AddPlayer( JOPlayer player )
        {
            _playersToInstruments[ player ] = player.PrimaryInstrument;
        }

        /// <summary>
        /// Adds a player to the program.
        /// </summary>
        /// <param name="player">The player to add.</param>
        /// <param name="instrument">The instrumen that the player will be playing.</param>
        virtual public void AddPlayer( JOPlayer player, JOInstrument instrument )
        {
            _playersToInstruments[ player ] = instrument;
        }

        /// <summary>
        /// Removes a player from the program.
        /// </summary>
        /// <param name="player">The player to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        virtual public bool RemovePlayer( JOPlayer player )
        {
            return _playersToInstruments.Remove( player );
        }
    }
}
