﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown instrument.
    /// </summary>
    internal class JOUnknownInstrument : JOInstrument
    {
        /// <summary>
        /// The name of the instrument.
        /// </summary>
        override public string Name
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Name )} on Unknown instrument" );
        }

        /// <summary>
        /// The aliases associated with this instrument.
        /// </summary>
        override public IReadOnlyCollection<JOInstrumentAlias> InstrumentAliases => null;

        internal JOUnknownInstrument()
        {
        }

        /// <summary>
        /// Adds an alias for this instrument.
        /// </summary>
        /// <param name="alias">The alias to add.</param>
        override public void AddInstrumentAlias( JOInstrumentAlias alias )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes an alias from this instument.
        /// </summary>
        /// <param name="alias">The alias to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        override public bool RemoveInstrumentAlias( JOInstrumentAlias alias )
        {
            throw new NotImplementedException();
        }
    }
}
