﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown piece.
    /// </summary>
    internal class JOUnknownPiece : JOPiece
    {
        /// <summary>
        /// The name of the piece.
        /// </summary>
        override public string Name
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Name )} on Unknown piece" );
        }

        /// <summary>
        /// The composer of the piece.
        /// </summary>
        override public string Composer
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Composer )} on Unknown piece" );
        }

        /// <summary>
        /// The library number of the piece.
        /// </summary>
        override public int LibraryNumber
        {
            get => 0;
            set => throw new Exception( $"Can't set {nameof( LibraryNumber )} on Unknown piece" );
        }

        /// <summary>
        /// Notes regarding this piece.
        /// </summary>
        override public string Notes
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Notes )} on Unknown piece" );
        }

        /// <summary>
        /// The list of parts that comprise this piece.
        /// </summary>
        override public IReadOnlyCollection<JOPart> Parts => null;

        internal JOUnknownPiece()
        {
        }

        /// <summary>
        /// Adds a part to this piece.
        /// </summary>
        /// <param name="part">The part to add.</param>
        override public void AddPart( JOPart part )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes a part from this piece.
        /// </summary>
        /// <param name="part">The part to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        override public bool RemovePart( JOPart part )
        {
            throw new NotImplementedException();
        }
    }
}
