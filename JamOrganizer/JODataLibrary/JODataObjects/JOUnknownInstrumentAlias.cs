﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown instrument alias.
    /// </summary>
    internal class JOUnknownInstrumentAlias: JOInstrumentAlias
    {
        /// <summary>
        /// The name of the instrument alias.
        /// </summary>
        override public string Name
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Name )} on Unknown instrument alias" );
        }

        internal JOUnknownInstrumentAlias()
        {
        }
    }
}
