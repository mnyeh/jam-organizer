﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding a piece.
    /// </summary>
    public class JOPiece : JODataObjectBaseClass
    {
        /// <summary>
        /// The name of the piece.
        /// </summary>
        virtual public string Name { get; set; }

        /// <summary>
        /// The composer of the piece.
        /// </summary>
        virtual public string Composer { get; set; }

        /// <summary>
        /// The library number of the piece.
        /// </summary>
        virtual public int LibraryNumber { get; set; }

        /// <summary>
        /// Notes regarding this piece.
        /// </summary>
        virtual public string Notes { get; set; }

        private readonly List<JOPart> _parts = new List<JOPart>();
        /// <summary>
        /// The list of parts that comprise this piece.
        /// </summary>
        virtual public IReadOnlyCollection<JOPart> Parts => _parts;

        public JOPiece( Guid guid, string name, string composer, int libraryNumber, string notes = "" ) : base( guid )
        {
            Name = name;
            Composer = composer;
            LibraryNumber = libraryNumber;
            Notes = notes;
        }

        protected JOPiece() : base( Guid.Empty )
        {
        }

        /// <summary>
        /// Adds a part to this piece.
        /// </summary>
        /// <param name="part">The part to add.</param>
        virtual public void AddPart( JOPart part )
        {
            _parts.Add( part );
        }

        /// <summary>
        /// Removes a part from this piece.
        /// </summary>
        /// <param name="part">The part to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        virtual public bool RemovePart( JOPart part )
        {
            // Remove the ref from any parts that are holding it
            if( part.IsAlternatePart )
            {
                foreach( JOPart p in _parts )
                {
                    p.RemoveAlternatePart( part );
                }
            }

            return _parts.Remove( part );
        }
    }
}
