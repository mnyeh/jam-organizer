﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding an instrument alias.
    /// </summary>
    public class JOInstrumentAlias : JODataObjectBaseClass
    {
        /// <summary>
        /// The name of the instrument alias.
        /// </summary>
        virtual public string Name { get; set; }

        public JOInstrumentAlias( Guid guid, string name ) : base( guid )
        {
            Name = name;
        }

        protected JOInstrumentAlias() : base( Guid.Empty )
        {
        }
    }
}
