﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JODataLibrary.JODataManagers;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown part for a piece.
    /// </summary>
    internal class JOUnknownPart : JOPart
    {
        /// <summary>
        /// The filename of the part.
        /// </summary>
        override public string Filename
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Filename )} on Unknown part" );
        }

        /// <summary>
        /// The instrument that this part is for.
        /// </summary>
        override public JOInstrument Instrument
        {
            get => JOInstrumentManager.UnknownInstrument;
            set => throw new Exception( $"Can't set {nameof( Instrument )} on Unknown part" );
        }

        /// <summary>
        /// Whether or not the part is an alternate part.
        /// </summary>
        override public bool IsAlternatePart
        {
            get => false;
            set => throw new Exception( $"Can't set {nameof( IsAlternatePart )} on Unknown part" );
        }

        /// <summary>
        /// Notes regarding this part.
        /// </summary>
        override public string Notes
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Notes )} on Unknown part" );
        }

        /// <summary>
        /// The list of alternate parts.
        /// </summary>
        override public IReadOnlyCollection<JOPart> AlternateParts => null;

        internal JOUnknownPart()
        {
        }
        
        /// <summary>
        /// Adds an alternate part to this part.
        /// </summary>
        /// <param name="alternatePart">The alternate part to add.</param>
        override public void AddAlternatePart( JOPart alternatePart )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes an alternate part from this part.
        /// </summary>
        /// <param name="alternatePart">The alternate part to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        override public bool RemoveAlternatePart( JOPart alternatePart )
        {
            throw new NotImplementedException();
        }
    }
}
