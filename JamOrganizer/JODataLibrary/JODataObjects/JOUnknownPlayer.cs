﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JODataLibrary.JODataManagers;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown player.
    /// </summary>
    internal class JOUnknownPlayer : JOPlayer
    {
        /// <summary>
        /// The full name of the player.
        /// </summary>
        override public string Name
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Name )} on Unknown player" );
        }

        /// <summary>
        /// The primary instrument of the player.
        /// </summary>
        override public JOInstrument PrimaryInstrument
        {
            get => JOInstrumentManager.UnknownInstrument;
            set => throw new Exception( $"Can't set {nameof( PrimaryInstrument )} on Unknown player" );
        }

        /// <summary>
        /// The email address of the player.
        /// </summary>
        override public string Email
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Email )} on Unknown player" );
        }

        /// <summary>
        /// Notes regarding the player.
        /// </summary>
        override public string Notes
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Notes )} on Unknown player" );
        }

        internal JOUnknownPlayer()
        {
        }
    }
}
