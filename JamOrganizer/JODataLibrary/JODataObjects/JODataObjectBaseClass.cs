﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Base class that defines all common behavior among data objects
    /// </summary>
    public abstract class JODataObjectBaseClass
    {
        /// <summary>
        /// The GUID of the data object.
        /// </summary>
        virtual public Guid Guid { get; }

        internal JODataObjectBaseClass( Guid guid )
        {
            Guid = guid;
        }
    }
}
