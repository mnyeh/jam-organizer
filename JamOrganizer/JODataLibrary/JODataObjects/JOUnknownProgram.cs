﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing (default) data regarding an unknown program.
    /// </summary>
    public class JOUnknownProgram : JOProgram
    {
        /// <summary>
        /// The name of the program.
        /// </summary>
        override public string Name
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Name )} on Unknown program" );
        }

        /// <summary>
        /// Whether or not the program is active.
        /// </summary>
        override public bool Active
        {
            get => false;
            set => throw new Exception( $"Can't set {nameof( Active )} on Unknown program" );
        }

        /// <summary>
        /// Notes regarding the program.
        /// </summary>
        override public string Notes
        {
            get => "Unknown";
            set => throw new Exception( $"Can't set {nameof( Notes )} on Unknown program" );
        }

        /// <summary>
        /// The list of pieces for the program.
        /// </summary>
        override public IReadOnlyCollection<JOPiece> Pieces => null;

        /// <summary>
        /// The list of players for the program.
        /// </summary>
        override public IReadOnlyCollection<JOPlayer> Players => null;

        internal JOUnknownProgram()
        {
        }

        /// <summary>
        /// Returns the instrument that a player is playing for the program.
        /// </summary>
        /// <param name="player">The player to get the instrument for.</param>
        /// <returns>The instrument that a player is playing in the program.</returns>
        override public JOInstrument GetInstrumentForPlayer( JOPlayer player )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the instrument that the player is playing for the program.
        /// </summary>
        /// <param name="instrument"></param>
        /// <param name="player"></param>
        override public void SetInstrumentForPlayer( JOInstrument instrument, JOPlayer player )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds a piece to the program.
        /// </summary>
        /// <param name="piece">The piece to add.</param>
        override public void AddPiece( JOPiece piece )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes a piece from the program.
        /// </summary>
        /// <param name="piece">The piece to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        override public bool RemovePiece( JOPiece piece )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds a player to the program.
        /// </summary>
        /// <param name="player">The player to add.</param>
        override public void AddPlayer( JOPlayer player )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds a player to the program.
        /// </summary>
        /// <param name="player">The player to add.</param>
        /// <param name="instrument">The instrumen that the player will be playing.</param>
        override public void AddPlayer( JOPlayer player, JOInstrument instrument )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes a player from the program.
        /// </summary>
        /// <param name="player">The player to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        override public bool RemovePlayer( JOPlayer player )
        {
            throw new NotImplementedException();
        }
    }
}
