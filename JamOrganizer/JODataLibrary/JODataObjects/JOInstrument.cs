﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JODataLibrary.JODataObjects
{
    /// <summary>
    /// Class describing data regarding an instrument.
    /// </summary>
    public class JOInstrument : JODataObjectBaseClass
    {
        /// <summary>
        /// The name of the instrument.
        /// </summary>
        virtual public string Name { get; set; }

        private readonly List<JOInstrumentAlias> _instrumentAliases = new List<JOInstrumentAlias>();
        /// <summary>
        /// The aliases associated with this instrument.
        /// </summary>
        virtual public IReadOnlyCollection<JOInstrumentAlias> InstrumentAliases => _instrumentAliases;

        public JOInstrument( Guid guid, string name ) : base( guid )
        {
            Name = name;
        }

        protected JOInstrument() : base( Guid.Empty )
        {
        }

        /// <summary>
        /// Adds an alias for this instrument.
        /// </summary>
        /// <param name="alias">The alias to add.</param>
        virtual public void AddInstrumentAlias( JOInstrumentAlias alias )
        {
            _instrumentAliases.Add( alias );
        }

        /// <summary>
        /// Removes an alias from this instument.
        /// </summary>
        /// <param name="alias">The alias to remove.</param>
        /// <returns>True if the removal was successful, false otherwise.</returns>
        virtual public bool RemoveInstrumentAlias( JOInstrumentAlias alias )
        {
            return _instrumentAliases.Remove( alias );
        }
    }
}
