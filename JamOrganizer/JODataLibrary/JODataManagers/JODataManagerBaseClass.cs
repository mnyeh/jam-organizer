﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace JODataLibrary.JODataManagers
{
    /// <summary>
    /// Base class that defines all common behavior among data managers
    /// </summary>
    public abstract class JODataManagerBaseClass
    {
        protected JODataProvider _provider;

        public JODataManagerBaseClass( JODataProvider provider )
        {
            _provider = provider;
        }

        /// <summary>
        /// Method to load a manager's data from the database
        /// </summary>
        /// <param name="connection">the database connection to read from</param>
        internal abstract void LoadFromDatabase( SQLiteConnection connection );

        /// <summary>
        /// Method to save a manager's data to the database
        /// </summary>
        /// <param name="connection">the database connection to save to</param>
        internal abstract void SaveToDatabase( SQLiteConnection connection );
    }
}
