﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using JODataLibrary.JODataObjects;

namespace JODataLibrary.JODataManagers
{
    internal class JOInstrumentManager : JODataManagerBaseClass
    {
        static internal JOInstrument UnknownInstrument = new JOUnknownInstrument();

        internal List<JOInstrument> Instruments { get; } = new List<JOInstrument>();

        internal JOInstrumentManager( JODataProvider provider ) : base( provider )
        {
        }

        internal void AddInstrument( JOInstrument instrument )
        {
            Instruments.Add( instrument );
        }

        internal bool RemoveInstrument( JOInstrument instrument )
        {
            // Remove instrument from any parts
            foreach( JOPart part in _provider.Pieces.SelectMany( piece => piece.Parts ).Where( part => part.Instrument.Guid == instrument.Guid ) )
            {
                part.Instrument = UnknownInstrument;
            }

            // Remove instrument from any players
            foreach( JOPlayer player in _provider.Players.Where( p => p.PrimaryInstrument.Guid == instrument.Guid ) )
            {
                player.PrimaryInstrument = UnknownInstrument;
            }

            // Remove instrument from any programs
            foreach( JOProgram program in _provider.Programs )
            {
                foreach( JOPlayer player in program.Players.Where( p => program.GetInstrumentForPlayer( p ).Guid == instrument.Guid ).ToList() )
                {
                    program.SetInstrumentForPlayer( UnknownInstrument, player );
                }
            }

            return Instruments.Remove( instrument );
        }

        #region SQLite
        internal override void LoadFromDatabase( SQLiteConnection connection )
        {
            // Read from instruments table
            string objectsSelectionCommand = "SELECT * FROM instruments";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        AddInstrument( new JOInstrument( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                           reader.GetString( reader.GetOrdinal( "name" ) ) ) );
                    }
                }
            }

            // Read from instrument alias table
            List<JOInstrumentAlias> instrumentAliases = new List<JOInstrumentAlias>();
            objectsSelectionCommand = "SELECT * FROM instrument_aliases";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        instrumentAliases.Add( new JOInstrumentAlias( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                                      reader.GetString( reader.GetOrdinal( "name" ) ) ) );
                    }
                }
            }

            // Read from instrument to instrument alias table and assign aliases to instruments
            objectsSelectionCommand = "SELECT * FROM instrument_to_instrument_aliases";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid instrumentGuid = reader.GetGuid( reader.GetOrdinal( "instrumentGuid" ) );
                        Guid aliasGuid = reader.GetGuid( reader.GetOrdinal( "aliasGuid" ) );

                        Instruments.Single( instrument => instrument.Guid == instrumentGuid ).AddInstrumentAlias( instrumentAliases.Single( alias => alias.Guid == aliasGuid ) );
                    }
                }
            }
        }

        internal override void SaveToDatabase( SQLiteConnection connection )
        {
            // Make the instruments table
            string tableCreationCommand = "CREATE TABLE instruments (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                                    "name STRING NOT NULL)";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );
            
            // Make the instrument aliases table
            tableCreationCommand = "CREATE TABLE instrument_aliases (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                                    "name STRING NOT NULL)";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Make the instrument to instrument aliases table
            tableCreationCommand = "CREATE TABLE instrument_to_instrument_aliases (instrumentGuid VARCHAR(20) NOT NULL, " +
                                                                                    "aliasGuid VARCHAR(20) NOT NULL, " +
                                                                                    "PRIMARY KEY (instrumentGuid, aliasGuid), " +
                                                                                    "FOREIGN KEY (instrumentGuid) REFERENCES instruments (guid), " +
                                                                                    "FOREIGN KEY (aliasGuid) REFERENCES instrument_aliases (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Collect object data in SQLite insertion form
            List<string> instrumentData = new List<string>();
            List<string> aliasData = new List<string>();
            List<string> instrumentToAliasData = new List<string>();
            const string instrumentInsertPrefix = "INSERT INTO instruments (guid, name) ";
            const string aliasInsertPrefix = "INSERT INTO instrument_aliases (guid, name) ";
            const string instrumentToAliasInsertPrefix = "INSERT INTO instrument_to_instrument_aliases (instrumentGuid, aliasGuid) ";
            
            foreach( JOInstrument instrument in Instruments )
            {
                instrumentData.Add( instrumentInsertPrefix + $"VALUES ('{instrument.Guid}', '{instrument.Name}')" );
                foreach( JOInstrumentAlias alias in instrument.InstrumentAliases )
                {
                    aliasData.Add( aliasInsertPrefix + $"VALUES ('{alias.Guid}', '{alias.Name}')" );
                    instrumentToAliasData.Add( instrumentToAliasInsertPrefix + $"VALUES ('{instrument.Guid}', '{alias.Guid}')" );
                }
            }

            // Populate the tables with their data
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, instrumentData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, aliasData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, instrumentToAliasData );
        }
        #endregion

        public override string ToString()
        {
            string str = $"Instruments:{Environment.NewLine}";
            foreach( JOInstrument instrument in Instruments )
            {
                str += $"{instrument.Name} - {instrument.Guid}{Environment.NewLine}";
                str += $"  Aliases:  ";
                foreach( JOInstrumentAlias alias in instrument.InstrumentAliases )
                {
                    str += $"{alias.Name} ";
                }
                str += Environment.NewLine;
            }
            return str;
        }
    }
}
