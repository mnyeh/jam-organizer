﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using JODataLibrary.JODataObjects;

namespace JODataLibrary.JODataManagers
{
    internal class JOPieceManager : JODataManagerBaseClass
    {
        static internal JOPiece UnknownPiece = new JOUnknownPiece();

        internal List<JOPiece> Pieces { get; } = new List<JOPiece>();

        internal JOPieceManager( JODataProvider provider ) : base( provider )
        {
        }

        internal void AddPiece( JOPiece piece )
        {
            Pieces.Add( piece );
        }

        internal bool RemovePiece( JOPiece piece )
        {
            // Remove piece from any programs
            foreach( JOProgram program in _provider.Programs )
            {
                JOPiece pieceToDelete = program.Pieces.SingleOrDefault( p => p.Guid == piece.Guid );
                if( null != pieceToDelete )
                {
                    program.RemovePiece( pieceToDelete );
                }
            }

            return Pieces.Remove( piece );
        }

        #region SQLite
        internal override void LoadFromDatabase( SQLiteConnection connection )
        {
            // Read from pieces table
            string objectsSelectionCommand = "SELECT * FROM pieces";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        AddPiece( new JOPiece( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                   reader.GetString( reader.GetOrdinal( "name" ) ),
                                                   reader.GetString( reader.GetOrdinal( "composer" ) ),
                                                   reader.GetInt32( reader.GetOrdinal( "libraryNumber" ) ),
                                                   reader.GetString( reader.GetOrdinal( "notes" ) ) ) );
                    }
                }
            }

            // Read from parts table
            List<JOPart> parts = new List<JOPart>();
            objectsSelectionCommand = "SELECT * FROM parts";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid instrumentGuid = reader.GetGuid( reader.GetOrdinal( "instrument" ) );
                        JOInstrument instrument = instrumentGuid == Guid.Empty ? 
                                                  JOInstrumentManager.UnknownInstrument : 
                                                  _provider.Instruments.Single( i => i.Guid == instrumentGuid );

                        parts.Add( new JOPart( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                               reader.GetString( reader.GetOrdinal( "filename" ) ),
                                                               instrument,
                                                               reader.GetBoolean( reader.GetOrdinal( "isAlternatePart" ) ),
                                                               reader.GetString( reader.GetOrdinal( "notes" ) ) ) );
                    }
                }
            }

            // Read from pieces to parts table and assign parts to pieces
            objectsSelectionCommand = "SELECT * FROM pieces_to_parts";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid pieceGuid = reader.GetGuid( reader.GetOrdinal( "pieceGuid" ) );
                        Guid partGuid = reader.GetGuid( reader.GetOrdinal( "partGuid" ) );

                        Pieces.Single( piece => piece.Guid == pieceGuid ).AddPart( parts.Single( part => part.Guid == partGuid ) );
                    }
                }
            }
        }

        internal override void SaveToDatabase( SQLiteConnection connection )
        {
            // Make the pieces table
            string tableCreationCommand = "CREATE TABLE pieces (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                               "name STRING NOT NULL, " +
                                                               "composer STRING NOT NULL, " +
                                                               "libraryNumber INT NOT NULL, " +
                                                               "notes STRING)";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Make the parts table
            tableCreationCommand = "CREATE TABLE parts (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                       "filename STRING NOT NULL, " +
                                                       "instrument VARCHAR(20) NOT NULL, " +
                                                       "isAlternatePart INT(1) NOT NULL, " +
                                                       "notes STRING, " +
                                                       "FOREIGN KEY (instrument) REFERENCES instruments (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Make the pieces to parts table
            tableCreationCommand = "CREATE TABLE pieces_to_parts (pieceGuid VARCHAR(20) NOT NULL, " +
                                                                 "partGuid VARCHAR(20) NOT NULL, " +
                                                                 "PRIMARY KEY (pieceGuid, partGuid), " +
                                                                 "FOREIGN KEY (pieceGuid) REFERENCES pieces (guid), " +
                                                                 "FOREIGN KEY (partGuid) REFERENCES parts (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Collect object data in SQLite insertion form
            List<string> pieceData = new List<string>();
            List<string> partData = new List<string>();
            List<string> pieceToPartData = new List<string>();
            const string pieceInsertPrefix = "INSERT INTO pieces (guid, name, composer, libraryNumber, notes) ";
            const string partInsertPrefix = "INSERT INTO parts (guid, filename, instrument, isAlternatePart, notes) ";
            const string pieceToPartInsertPrefix = "INSERT INTO pieces_to_parts (pieceGuid, partGuid) ";

            foreach( JOPiece piece in Pieces )
            {
                pieceData.Add( pieceInsertPrefix + $"VALUES ('{piece.Guid}', '{piece.Name}', '{piece.Composer}', {piece.LibraryNumber}, '{piece.Notes}')" );

                foreach( JOPart part in piece.Parts )
                {
                    partData.Add( partInsertPrefix + $"VALUES ('{part.Guid}', '{part.Filename}', '{part.Instrument.Guid}', {part.IsAlternatePart}, '{part.Notes}')" );
                    pieceToPartData.Add( pieceToPartInsertPrefix + $"VALUES ('{piece.Guid}', '{part.Guid}')" );
                }
            }

            // Populate the tables with their data
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, pieceData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, partData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, pieceToPartData );
        }
        #endregion

        public override string ToString()
        {
            string str = $"Pieces:{Environment.NewLine}";
            foreach( JOPiece piece in Pieces )
            {
                str += $"{piece.Name} by {piece.Composer} ({piece.LibraryNumber}) - {piece.Guid}{Environment.NewLine}";
                str += $"  Notes: {piece.Notes}{Environment.NewLine}";
                str += $"  Parts:{Environment.NewLine}";

                foreach( JOPart part in piece.Parts )
                {
                    string isAlternate = part.IsAlternatePart ? "*" : string.Empty;
                    str += $"    {part.Instrument.Name}{isAlternate} - {part.Guid}{Environment.NewLine}";
                    str += $"    Filename:  {part.Filename}{Environment.NewLine}";
                    str += $"    Notes:  {part.Notes}{Environment.NewLine}";
                }
                str += Environment.NewLine;
            }

            return str;
        }
    }
}
