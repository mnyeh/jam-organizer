﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using JODataLibrary.JODataObjects;

namespace JODataLibrary.JODataManagers
{
    internal class JOProgramManager : JODataManagerBaseClass
    {
        static internal JOProgram UnknownProgram = new JOUnknownProgram();

        internal List<JOProgram> Programs { get; } = new List<JOProgram>();

        internal JOProgramManager( JODataProvider provider ) : base( provider )
        {
        }

        internal void AddProgram( JOProgram program )
        {
            Programs.Add( program );
        }

        internal bool RemoveProgram( JOProgram program )
        {
            return Programs.Remove( program );
        }

        #region SQLite
        internal override void LoadFromDatabase( SQLiteConnection connection )
        {
            // Read from programs table
            string objectsSelectionCommand = "SELECT * FROM programs";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        AddProgram( new JOProgram( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                   reader.GetString( reader.GetOrdinal( "name" ) ),
                                                   reader.GetBoolean( reader.GetOrdinal( "active" ) ),
                                                   reader.GetString( reader.GetOrdinal( "notes" ) ) ) );
                    }
                }
            }

            // Read from programs to pieces table and assign pieces to programs
            objectsSelectionCommand = "SELECT * FROM programs_to_pieces";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid programGuid = reader.GetGuid( reader.GetOrdinal( "programGuid" ) );
                        Guid pieceGuid = reader.GetGuid( reader.GetOrdinal( "pieceGuid" ) );
                        JOPiece piece = pieceGuid == Guid.Empty ? JOPieceManager.UnknownPiece : _provider.Pieces.Single( p => p.Guid == pieceGuid );

                        Programs.Single( program => program.Guid == programGuid ).AddPiece( piece );
                    }
                }
            }

            // Read from programs to players and instruments table and assign players to programs
            objectsSelectionCommand = "SELECT * FROM programs_to_players_and_instruments";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid programGuid = reader.GetGuid( reader.GetOrdinal( "programGuid" ) );
                        Guid playerGuid = reader.GetGuid( reader.GetOrdinal( "playerGuid" ) );
                        Guid instrumentGuid = reader.GetGuid( reader.GetOrdinal( "instrumentGuid" ) );

                        JOPlayer player = playerGuid == Guid.Empty ? JOPlayerManager.UnknkownPlayer : _provider.Players.Single( p => p.Guid == playerGuid );
                        JOInstrument instrument;
                        if( instrumentGuid == Guid.Empty )
                        {
                            instrument = JOInstrumentManager.UnknownInstrument;
                        }
                        else if( instrumentGuid == player.PrimaryInstrument.Guid )
                        {
                            instrument = player.PrimaryInstrument;
                        }
                        else
                        {
                            instrument = _provider.Instruments.Single( i => i.Guid == instrumentGuid );
                        }

                        Programs.Single( program => program.Guid == programGuid ).AddPlayer( player, instrument );
                    }
                }
            }
        }

        internal override void SaveToDatabase( SQLiteConnection connection )
        {
            // Make the programs table
            string tableCreationCommand = "CREATE TABLE programs (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                                 "name STRING NOT NULL, " +
                                                                 "active INT(1) NOT NULL, " +
                                                                 "notes STRING)";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );
            
            // Make the programs to pieces table
            tableCreationCommand = "CREATE TABLE programs_to_pieces (programGuid VARCHAR(20) NOT NULL, " +
                                                                    "pieceGuid VARCHAR(20) NOT NULL, " +
                                                                    "PRIMARY KEY (programGuid, pieceGuid), " +
                                                                    "FOREIGN KEY (programGuid) REFERENCES programs (guid), " +
                                                                    "FOREIGN KEY (pieceGuid) REFERENCES pieces (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Make the programs to players and instruments table
            tableCreationCommand = "CREATE TABLE programs_to_players_and_instruments (programGuid VARCHAR(20) NOT NULL, " +
                                                                                     "playerGuid VARCHAR(20) NOT NULL, " +
                                                                                     "instrumentGuid VARCHAR(20) NOT NULL, " +
                                                                                     "PRIMARY KEY (programGuid, playerGuid, instrumentGuid), " +
                                                                                     "FOREIGN KEY (programGuid) REFERENCES programs (guid), " +
                                                                                     "FOREIGN KEY (playerGuid) REFERENCES players (guid), " +
                                                                                     "FOREIGN KEY (instrumentGuid) REFERENCES instruments (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );

            // Collect object data in SQLite insertion form
            List<string> programData = new List<string>();
            List<string> pieceData = new List<string>();
            List<string> programPlayerInstrumentData = new List<string>();
            const string programInsertPrefix = "INSERT INTO programs (guid, name, active, notes) ";
            const string programToPieceInsertPrefix = "INSERT INTO programs_to_pieces (programGuid, pieceGuid) ";
            const string programToPlayerAndInstrumentInsertPrefix = "INSERT INTO programs_to_players_and_instruments (programGuid, playerGuid, instrumentGuid) ";

            foreach( JOProgram program in Programs )
            {
                programData.Add( programInsertPrefix + $"VALUES ('{program.Guid}', '{program.Name}', {program.Active}, '{program.Notes}')" );

                foreach( JOPiece piece in program.Pieces )
                {
                    pieceData.Add( programToPieceInsertPrefix + $"VALUES ('{program.Guid}', '{piece.Guid}')" );
                }

                foreach( JOPlayer player in program.Players )
                {
                    programPlayerInstrumentData.Add( programToPlayerAndInstrumentInsertPrefix + $"VALUES ('{program.Guid}', '{player.Guid}', '{program.GetInstrumentForPlayer( player ).Guid}')" );
                }
            }

            // Populate the tables with their data
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, programData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, pieceData );
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, programPlayerInstrumentData );
        }
        #endregion

        public override string ToString()
        {
            string str = $"Programs:{Environment.NewLine}";
            foreach( JOProgram program in Programs )
            {
                str += $"{program.Name} - {program.Guid}{Environment.NewLine}";
                str += $"  Active: {program.Active}{Environment.NewLine}";
                str += $"  Notes: {program.Notes}{Environment.NewLine}";
                str += "  Pieces:  ";

                foreach( JOPiece piece in program.Pieces )
                {
                    str += $"{piece.Name}; ";
                }
                str += Environment.NewLine;

                str += "  Players:  ";
                foreach( JOPlayer player in program.Players )
                {
                    str += $"{player.Name} playing {program.GetInstrumentForPlayer( player ).Name}; ";
                }
                str += Environment.NewLine;
            }

            return str;
        }
    }
}
