﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace JODataLibrary.JODataManagers
{
    internal static class JODataManagerSQLiteUtilities
    {
        internal static void ExecuteNonQuery( SQLiteConnection connection, string commandText )
        {
            using( SQLiteCommand command = new SQLiteCommand( commandText, connection ) )
            {
                command.ExecuteNonQuery();
            }
        }

        internal static void ExecuteMultipleNonQuery( SQLiteConnection connection, IEnumerable<string> commandsText )
        {
            using( SQLiteTransaction transaction = connection.BeginTransaction() )
            using( SQLiteCommand command = connection.CreateCommand() )
            {
                command.Transaction = transaction;

                foreach( string commandString in commandsText )
                {
                    command.CommandText = commandString;
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        internal static SQLiteDataReader ExecuteReader( SQLiteCommand command )
        {
            SQLiteDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
            }
            catch( SQLiteException e ) when ( e.Message.Contains( "no such table" ) )
            {
                // TODO:  If the table fails to load and throws this, we don't really care because it will 
                // get saved properly.  This is a dumb way of catching this error.  Readdress this.
            }

            return reader;
        }
    }
}
