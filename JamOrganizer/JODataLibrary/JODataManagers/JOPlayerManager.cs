﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using JODataLibrary.JODataObjects;

namespace JODataLibrary.JODataManagers
{
    internal class JOPlayerManager : JODataManagerBaseClass
    {
        static internal JOPlayer UnknkownPlayer = new JOUnknownPlayer();

        internal List<JOPlayer> Players { get; } = new List<JOPlayer>();

        internal JOPlayerManager( JODataProvider provider ) : base( provider )
        {
        }

        internal void AddPlayer( JOPlayer Player )
        {
            Players.Add( Player );
        }

        internal bool RemovePlayer( JOPlayer player )
        {
            // Remove player from any programs
            foreach( JOProgram program in _provider.Programs )
            {
                program.RemovePlayer( player );
            }

            return Players.Remove( player );
        }

        #region SQLite
        internal override void LoadFromDatabase( SQLiteConnection connection )
        {
            // Read from Players table
            string objectsSelectionCommand = "SELECT * FROM players";
            using( SQLiteCommand command = new SQLiteCommand( objectsSelectionCommand, connection ) )
            {
                SQLiteDataReader reader = JODataManagerSQLiteUtilities.ExecuteReader( command );
                if( reader != null )
                {
                    while( reader.Read() )
                    {
                        Guid instrumentGuid = reader.GetGuid( reader.GetOrdinal( "primaryInstrument" ) );
                        JOInstrument instrument = instrumentGuid == Guid.Empty ?
                                                  JOInstrumentManager.UnknownInstrument :
                                                  _provider.Instruments.Single( i => i.Guid == instrumentGuid );

                        AddPlayer( new JOPlayer( reader.GetGuid( reader.GetOrdinal( "guid" ) ),
                                                 reader.GetString( reader.GetOrdinal( "name" ) ),
                                                 instrument,
                                                 reader.GetString( reader.GetOrdinal( "email" ) ),
                                                 reader.GetString( reader.GetOrdinal( "notes" ) ) ) );
                    }
                }
            }
        }

        internal override void SaveToDatabase( SQLiteConnection connection )
        {
            // Make the Players table
            const string tableCreationCommand = "CREATE TABLE Players (guid VARCHAR(20) NOT NULL PRIMARY KEY, " +
                                                                      "name STRING NOT NULL, " +
                                                                      "primaryInstrument VARCHAR(20) NOT NULL, " +
                                                                      "email STRING, " +
                                                                      "notes STRING, " +
                                                                      "FOREIGN KEY (primaryInstrument) REFERENCES instruments (guid))";
            JODataManagerSQLiteUtilities.ExecuteNonQuery( connection, tableCreationCommand );
            
            // Collect object data in SQLite insertion form
            List<string> PlayerData = new List<string>();
            const string playerInsertPrefix = "INSERT INTO Players (guid, name, primaryInstrument, email, notes) ";

            foreach( JOPlayer player in Players )
            {
                PlayerData.Add( playerInsertPrefix + $"VALUES ('{player.Guid}', '{player.Name}', '{player.PrimaryInstrument.Guid}', '{player.Email}', '{player.Notes}')" );
            }

            // Populate the tables with their data
            JODataManagerSQLiteUtilities.ExecuteMultipleNonQuery( connection, PlayerData );
        }
        #endregion

        public override string ToString()
        {
            string str = $"Players:{Environment.NewLine}";
            foreach( JOPlayer player in Players )
            {
                str += $"{player.Name} playing {player.PrimaryInstrument.Name} - {player.Guid}{Environment.NewLine}";
                str += $"  Email:  {player.Email}{Environment.NewLine}";
                str += $"  Notes:  {player.Notes}{Environment.NewLine}";
            }
            return str;
        }
    }
}
