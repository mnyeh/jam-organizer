﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for pieces
    /// </summary>
    public class PiecesPaneViewModel : JODOCollectionViewModelBase
    {
        public override string Title => "Pieces";

        private JOPart _selectedPart;
        /// <summary>
        /// The currently selected part
        /// </summary>
        public JOPart SelectedPart
        {
            get => _selectedPart;
            set { SetProperty( ref _selectedPart, value ); }
        }

        private JOPiece _selectedPiece;
        /// <summary>
        /// The currently selected piece
        /// </summary>
        public JOPiece SelectedPiece
        {
            get => _selectedPiece;
            set
            {
                if( SetProperty( ref _selectedPiece, value ) )
                {
                    // Try to select the same part in the new selected piece
                    //JM:  This isn't working - might have to do this as a response to the SfDataGrid's SelectedItem callback
                    SelectedPart = _selectedPiece?.Parts.FirstOrDefault( p => p.Instrument.Name == _selectedPart?.Instrument.Name );
                }
            }
        }

        private ObservableCollection<JOPiece> _pieces;
        /// <summary>
        /// The collection of pieces
        /// </summary>
        public ObservableCollection<JOPiece> Pieces
        {
            get { return _pieces; }
            set { _pieces = value; }
        }

        public PiecesPaneViewModel( JODataProvider dataProvider ) : base( dataProvider )
        {
            //JM:  this is wrong - we want to tie the collection to the model
            Pieces = new ObservableCollection<JOPiece>( dataProvider.Pieces );

            string str = $"Pieces:{Environment.NewLine}";
            foreach( JOPiece piece in JODataProvider.Pieces )
            {
                str += $"{piece.Name} by {piece.Composer} ({piece.LibraryNumber}) - {piece.Guid}{Environment.NewLine}";
                str += $"  Notes: {piece.Notes}{Environment.NewLine}";
                str += $"  Parts:{Environment.NewLine}";

                foreach( JOPart part in piece.Parts )
                {
                    string isAlternate = part.IsAlternatePart ? "*" : string.Empty;
                    str += $"    {part.Instrument.Name}{isAlternate} - {part.Guid}{Environment.NewLine}";
                    str += $"    Filename:  {part.Filename}{Environment.NewLine}";
                    str += $"    Notes:  {part.Notes}{Environment.NewLine}";
                }
                str += Environment.NewLine;
            }
            Output = str;
        }
    }
}
