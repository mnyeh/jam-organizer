﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using JamOrganizer.Utilities;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for a JOInstrument
    /// </summary>
    public class InstrumentViewModel : JODOViewModelBase<JOInstrument>
    {
        /// <summary>
        /// The Name of the instrument
        /// </summary>
        public string Name
        {
            get { return _model.Name; }
            set
            {
                if( _model.Name == value ) return;
                _model.Name = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<InstrumentAliasViewModel> _instrumentAliases = new ObservableCollection<InstrumentAliasViewModel>();
        /// <summary>
        /// The collection of instrument aliases
        /// </summary>
        public ObservableCollection<InstrumentAliasViewModel> InstrumentAliases
        {
            get { return _instrumentAliases; }
            set { _instrumentAliases = value; }
        }

        private InstrumentAliasViewModel _selectedInstrumentAlias;
        public InstrumentAliasViewModel SelectedInstrumentAlias
        {
            get { return _selectedInstrumentAlias; }
            set { SetProperty( ref _selectedInstrumentAlias, value ); }
        }

        private RelayCommand<InstrumentAliasViewModel> _deleteInstrumentAliasCommand;
        /// <summary>
        /// The command to delete an instrument alias
        /// </summary>
        public RelayCommand<InstrumentAliasViewModel> DeleteInstrumentAliasCommand
        {
            get
            {
                if( _deleteInstrumentAliasCommand == null )
                {
                    _deleteInstrumentAliasCommand = new RelayCommand<InstrumentAliasViewModel>( RemoveInstrumentAlias );
                }

                return _deleteInstrumentAliasCommand;
            }
        }

        public InstrumentViewModel( JODataProvider dataProvider, JOInstrument instrument ) : base( dataProvider, instrument )
        {
            foreach( JOInstrumentAlias alias in instrument.InstrumentAliases )
            {
                InstrumentAliases.Add( new InstrumentAliasViewModel( dataProvider, alias ) );
            }
        }

        private void RemoveInstrumentAlias( InstrumentAliasViewModel alias )
        {
            if( alias != null )
            {
                InstrumentAliases.Remove( alias );
            }
        }
    }
}
