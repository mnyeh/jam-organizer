﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JODataLibrary;

namespace JamOrganizer.ViewModels
{
    public abstract class JODOCollectionViewModelBase : ViewModelBase
    {
        public abstract string Title { get; }

        protected JODataProvider JODataProvider { get; private set; }

        protected string _output;
        public string Output
        {
            get { return _output; }
            set { SetProperty( ref _output, value ); }
        }

        public JODOCollectionViewModelBase( JODataProvider dataProvider )
        {
            JODataProvider = dataProvider;
        }
    }
}
