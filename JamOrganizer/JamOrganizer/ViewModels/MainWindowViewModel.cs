﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using JODataLibrary;
using System.Collections;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for the main window
    /// </summary>
    public class MainWindowViewModel : ViewModelBase
    {
        /// <summary>
        /// The pieces pane
        /// </summary>
        public PiecesPaneViewModel PiecesViewModel { get; }

        /// <summary>
        /// The programs pane
        /// </summary>
        public ProgramsPaneViewModel ProgramsViewModel { get; }

        /// <summary>
        /// The players pane
        /// </summary>
        public PlayersPaneViewModel PlayersViewModel { get; }

        /// <summary>
        /// The instruments pane
        /// </summary>
        public InstrumentsPaneViewModel InstrumentsViewModel { get; }

        /// <summary>
        /// The test pane
        /// </summary>
        public TestViewModel TestViewModel { get; }

        /// <summary>
        /// List of all of the view models
        /// </summary>
        public IList ViewModels { get; }

        public MainWindowViewModel( JODataProvider dataProvider )
        {
            PiecesViewModel = new PiecesPaneViewModel( dataProvider );
            ProgramsViewModel = new ProgramsPaneViewModel( dataProvider );
            PlayersViewModel = new PlayersPaneViewModel( dataProvider );
            InstrumentsViewModel = new InstrumentsPaneViewModel( dataProvider );
            TestViewModel = new TestViewModel( dataProvider );

            ViewModels = new List<JODOCollectionViewModelBase>
            {
                PiecesViewModel,
                ProgramsViewModel,
                PlayersViewModel,
                InstrumentsViewModel,
                TestViewModel
            };
        }
    }
}
