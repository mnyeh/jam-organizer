﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using JamOrganizer.Utilities;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for the instrument pane
    /// </summary>
    public class InstrumentsPaneViewModel : JODOCollectionViewModelBase
    {
        public override string Title => "Instruments";

        private RelayCommand<InstrumentViewModel> _deleteInstrumentCommand;
        /// <summary>
        /// The command to delete an instrument
        /// </summary>
        public RelayCommand<InstrumentViewModel> DeleteInstrumentCommand
        {
            get
            {
                if( _deleteInstrumentCommand == null )
                {
                    _deleteInstrumentCommand = new RelayCommand<InstrumentViewModel>( DeleteInstrument );
                }

                return _deleteInstrumentCommand;
            }
        }

        private ObservableCollection<InstrumentViewModel> _instruments = new ObservableCollection<InstrumentViewModel>();
        /// <summary>
        /// The collection of instruments
        /// </summary>
        public ObservableCollection<InstrumentViewModel> Instruments
        {
            get { return _instruments; }
            set { _instruments = value; }
        }

        private InstrumentViewModel _selectedInstrument;
        public InstrumentViewModel SelectedInstrument
        {
            get { return _selectedInstrument; }
            set { SetProperty( ref _selectedInstrument, value ); }
        }

        public InstrumentsPaneViewModel( JODataProvider dataProvider ) : base( dataProvider )
        {
            string str = $"Instruments:{Environment.NewLine}";

            foreach( JOInstrument instrument in dataProvider.Instruments )
            {
                Instruments.Add( new InstrumentViewModel( dataProvider, instrument ) );

                str += $"{instrument.Name} - {instrument.Guid}{Environment.NewLine}";
                str += $"  Aliases:  ";
                foreach( JOInstrumentAlias alias in instrument.InstrumentAliases )
                {
                    str += $"{alias.Name} ";
                }
                str += Environment.NewLine;
            }
            Output = str;
        }

        /// <summary>
        /// Deletes an instrument from the collection.  We don't want to delete directly from the collection due to dependencies.
        /// </summary>
        /// <param name="instrument">The instrument to delete</param>
        private void DeleteInstrument( InstrumentViewModel instrument )
        {
            //JM:  implement dependency logic and surface window to user
            Instruments.Remove( instrument );
        }

        /// <summary>
        /// Creates a new instrument view model
        /// </summary>
        public InstrumentViewModel CreateInstrumentViewModel() => new InstrumentViewModel( JODataProvider, new JOInstrument( Guid.NewGuid(), "New Instrument" ) );

        /// <summary>
        /// Creates a new instrument alias view model
        /// </summary>
        public InstrumentAliasViewModel CreateInstrumentAliasViewModel() => new InstrumentAliasViewModel( JODataProvider, new JOInstrumentAlias( Guid.NewGuid(), "New Instrument" ) );
    }
}
