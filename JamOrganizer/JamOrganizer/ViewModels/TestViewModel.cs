﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;

namespace JamOrganizer.ViewModels
{
    public class TestViewModel : JODOCollectionViewModelBase
    {
        public override string Title => "Test";

        public TestViewModel( JODataProvider dataProvider ) : base( dataProvider )
        {
            //dataProvider.AddInstrument( new JOInstrument( Guid.NewGuid(), "Tenor Horn", 3 ) );
            //dataProvider.AddInstrument( new JOInstrument( Guid.NewGuid(), "Euphonium", 2 ) );
            JOInstrument baritone = dataProvider.Instruments.SingleOrDefault( instrument => instrument.Name == "Baritone" );
            JOInstrument euph = dataProvider.Instruments.SingleOrDefault( instrument => instrument.Name == "Euphonium" );
            JOPiece meFarting = dataProvider.Pieces.SingleOrDefault( piece => piece.Name == "Me Farting" );
            JOPlayer hernan = dataProvider.Players.SingleOrDefault( player => player.Name == "Hernan Troncoso" );
            JOProgram program = dataProvider.Programs.SingleOrDefault( p => p.Name == "Program Flies" );
            //dataProvider.AddInstrumentAlias( new JOInstrumentAlias( Guid.NewGuid(), "Daritone" ), baritone );
            //dataProvider.AddInstrumentAlias( new JOInstrumentAlias( Guid.NewGuid(), "Flaritone" ), baritone );
            //dataProvider.AddInstrumentAlias( new JOInstrumentAlias( Guid.NewGuid(), "Boophonium" ), euph );
            //dataProvider.AddPlayer( new JOPlayer( Guid.NewGuid(), "Hernan Troncoso", baritone, "hernan@gmail.com", "Librarian" ) );
            //dataProvider.AddPlayer( new JOPlayer( Guid.NewGuid(), "Yannick Innis", euph, "yannick@gmail.com", "Major Contributor to annual Blackest Band award" ) );

            JOPiece pi1 = new JOPiece( Guid.NewGuid(), "Me Farting", "Chopin", 1 );
            JOPart pa1 = new JOPart( Guid.NewGuid(), "barFile", baritone );
            JOPart pa2 = new JOPart( Guid.NewGuid(), "euphFile", euph );
            //dataProvider.AddPiece( pi1 );
            //pi1.AddPart( pa1 );
            //pi1.AddPart( pa2 );

            JOPiece pi2 = new JOPiece( Guid.NewGuid(), "Oreo Stuff", "Amber", 2, "Is this even real music?" );
            JOPart pa3 = new JOPart( Guid.NewGuid(), "barFile", baritone );
            JOPart pa4 = new JOPart( Guid.NewGuid(), "euphFile", euph );
            //dataProvider.AddPiece( pi2 );
            //pi2.AddPart( pa3 );
            //pi2.AddPart( pa4 );

            JOProgram pro = new JOProgram( Guid.NewGuid(), "Program Flies" );
            //dataProvider.AddProgram( pro );
            //pro.AddPiece( pi1 );
            //pro.AddPiece( pi2 );
            //pro.AddPlayer( dataProvider.Players.First( player => player.Name == "Yannick Innis" ) );
            //pro.AddPlayer( dataProvider.Players.First( player => player.Name == "Hernan Troncoso" ), dataProvider.Instruments.Single( instrument => instrument.Name == "Tenor Horn" ) );

            //dataProvider.RemoveInstrument( euph );
            //dataProvider.RemovePiece( meFarting );
            //dataProvider.RemovePlayer( hernan );
            //dataProvider.RemoveProgram( program );
            
            dataProvider.Save();

            Output = dataProvider.ToString();
        }

        public override string ToString()
        {
            return Output;
        }
    }
}
