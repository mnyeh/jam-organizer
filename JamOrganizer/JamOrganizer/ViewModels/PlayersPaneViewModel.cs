﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for players
    /// </summary>
    public class PlayersPaneViewModel : JODOCollectionViewModelBase
    {
        public override string Title => "Players";

        private ObservableCollection<JOPlayer> _players;
        /// <summary>
        /// The collection of players
        /// </summary>
        public ObservableCollection<JOPlayer> Players
        {
            get { return _players; }
            set { _players = value; }
        }

        public PlayersPaneViewModel( JODataProvider dataProvider ) : base( dataProvider )
        {
            //JM:  this is wrong - we want to tie the collection to the model
            Players = new ObservableCollection<JOPlayer>( dataProvider.Players );

            string str = $"Players:{Environment.NewLine}";
            foreach( JOPlayer player in JODataProvider.Players )
            {
                str += $"{player.Name} playing {player.PrimaryInstrument.Name} - {player.Guid}{Environment.NewLine}";
                str += $"  Email:  {player.Email}{Environment.NewLine}";
                str += $"  Notes:  {player.Notes}{Environment.NewLine}";
            }
            Output = str;
        }
    }
}
