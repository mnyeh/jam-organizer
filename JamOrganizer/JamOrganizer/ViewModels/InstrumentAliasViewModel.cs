﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    ///  View model for a JOInstrumentAlias
    /// </summary>
    public class InstrumentAliasViewModel : JODOViewModelBase<JOInstrumentAlias>
    {
        /// <summary>
        /// The name of the alias
        /// </summary>
        public string Name
        {
            get { return _model.Name; }
            set
            {
                if( _model.Name == value ) return;
                _model.Name = value;
                OnPropertyChanged();
            }
        }

        public InstrumentAliasViewModel( JODataProvider dataProvider, JOInstrumentAlias alias ) : base( dataProvider, alias )
        {
        }
    }
}
