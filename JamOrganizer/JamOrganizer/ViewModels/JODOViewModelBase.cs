﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JODataLibrary;
using JODataLibrary.JODataObjects;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// Base class of a view model wrapping a JODataObject
    /// </summary>
    public abstract class JODOViewModelBase : ViewModelBase
    {
        /// <summary>
        /// The data provider
        /// </summary>
        protected JODataProvider JODataProvider { get; private set; }

        public JODOViewModelBase( JODataProvider dataProvider )
        {
            JODataProvider = dataProvider;
        }
    }

    /// <summary>
    /// Generic base class of a view model wrapping a JODataObject
    /// </summary>
    /// <typeparam name="T">The JODataObject type</typeparam>
    public abstract class JODOViewModelBase<T> : JODOViewModelBase where T : JODataObjectBaseClass
    {
        /// <summary>
        /// Reference to the model being wrapped by the view model
        /// </summary>
        protected T _model;

        public JODOViewModelBase( JODataProvider dataProvider, T model ) : base( dataProvider )
        {
            _model = model;
        }
    }
}
