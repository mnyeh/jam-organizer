﻿using System;
using JODataLibrary;
using JODataLibrary.JODataObjects;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// View model for programs
    /// </summary>
    public class ProgramsPaneViewModel : JODOCollectionViewModelBase
    {
        public override string Title => "Programs";

        private JOProgram _selectedProgram;
        /// <summary>
        /// The currently selected program
        /// </summary>
        public JOProgram SelectedProgram
        {
            get => _selectedProgram;
            set { SetProperty( ref _selectedProgram, value ); }
        }

        private ObservableCollection<JOProgram> _programs;
        /// <summary>
        /// The collection of programs
        /// </summary>
        public ObservableCollection<JOProgram> Programs
        {
            get { return _programs; }
            set { _programs = value; }
        }

        public ProgramsPaneViewModel( JODataProvider dataProvider ) : base( dataProvider )
        {
            //JM:  this is wrong - we want to tie the collection to the model
            Programs = new ObservableCollection<JOProgram>( dataProvider.Programs );

            string str = $"Programs:{Environment.NewLine}";
            foreach( JOProgram program in JODataProvider.Programs )
            {
                str += $"{program.Name} - {program.Guid}{Environment.NewLine}";
                str += $"  Active: {program.Active}{Environment.NewLine}";
                str += $"  Notes: {program.Notes}{Environment.NewLine}";
                str += "  Pieces:  ";

                foreach( JOPiece piece in program.Pieces )
                {
                    str += $"{piece.Name}; ";
                }
                str += Environment.NewLine;

                str += "  Players:  ";
                foreach( JOPlayer player in program.Players )
                {
                    str += $"{player.Name} playing {program.GetInstrumentForPlayer( player ).Name}; ";
                }
                str += Environment.NewLine;
            }
            Output = str;
        }
    }
}
