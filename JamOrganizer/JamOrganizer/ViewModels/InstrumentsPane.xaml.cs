﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.UI.Xaml.Grid.Helpers;
using JODataLibrary.JODataObjects;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// Interaction logic for InstrumentsPane.xaml
    /// </summary>
    public partial class InstrumentsPane : UserControl
    {
        public InstrumentsPane()
        {
            InitializeComponent();
        }

        private void SetRowHeightByCellContent( object sender, QueryRowHeightEventArgs eventArgs )
        {
            SfDataGrid dataGrid = sender as SfDataGrid;
            if( dataGrid.GridColumnSizer.GetAutoRowHeight( eventArgs.RowIndex, new GridRowSizingOptions(), out double autoHeight ) )
            {
                // Special rules if it's the alias data grid that we're resizing
                if( dataGrid.Name == "AliasesDataGrid" )
                {
                    // QueryRowHeight resizes the header row too even if the height is set, so make sure the alias data grid's header height is always 0
                    if( eventArgs.RowIndex == dataGrid.GetHeaderIndex() )
                    {
                        eventArgs.Height = 0;
                        eventArgs.Handled = true;
                        return;
                    }

                    // If a row in the aliases is being requeried (e.g. an object was added or deleted), make sure the parent row in InstrumentsDataGrid resizes as well
                    if( InstrumentsDataGrid.SelectedIndex != -1 )
                    {
                        // Skip the header row (0) and the "Add New Row" row (1)
                        InstrumentsDataGrid.InvalidateRowHeight( InstrumentsDataGrid.SelectedIndex + 2 );
                        InstrumentsDataGrid.GetVisualContainer().InvalidateMeasureInfo();
                    }
                }

                // Tweak the auto height by 1.0 to remove the need for vertical scrollbars on content
                //JM:  hopefully this isn't necessary and can be reverted if we're just drawing the alias view
                eventArgs.Height = autoHeight + 1.0;
                eventArgs.Handled = true;
            }
        }

        private void InvalidateRowHeightOnCellEdit( object sender, CurrentCellEndEditEventArgs eventArgs )
        {
            SfDataGrid dataGrid = sender as SfDataGrid;
            dataGrid.InvalidateRowHeight( eventArgs.RowColumnIndex.RowIndex );
            dataGrid.GetVisualContainer().InvalidateMeasureInfo();
                
        }

        private void OnNewRowAdded( object sender, AddNewRowInitiatingEventArgs eventArgs )
        {
            // Replace the event args object with a new object depending which data grid fired the event
            eventArgs.NewObject = ( sender as SfDataGrid == InstrumentsDataGrid ) ? 
                ( DataContext as InstrumentsPaneViewModel ).CreateInstrumentViewModel() as object : 
                ( DataContext as InstrumentsPaneViewModel ).CreateInstrumentAliasViewModel() as object;
        }
    }
}
