﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace JamOrganizer.ViewModels
{
    /// <summary>
    /// Base class for all view models.  Shamelessly stolen from The Internet.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( [CallerMemberName] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        protected virtual bool SetProperty<T>( ref T storage, T value, [CallerMemberName] string propertyName = null )
        {
            if( EqualityComparer<T>.Default.Equals( storage, value ) )
            {
                return false;
            }

            storage = value;
            OnPropertyChanged( propertyName );
            return true;
        }
    }
}
