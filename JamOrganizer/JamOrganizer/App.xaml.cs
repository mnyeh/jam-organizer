﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Syncfusion.Licensing;
using JODataLibrary;
using JamOrganizer.ViewModels;

namespace JamOrganizer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            SyncfusionLicenseProvider.RegisterLicense( "ODUwOTFAMzEzNzJlMzEyZTMwUGJGWHp0MDZCYURkeTFUZ1ovcndSdThUTlI0WlZYdFdyMGVzdElHR2FTVT0" );
        }

        protected override void OnStartup( StartupEventArgs e )
        {
            base.OnStartup( e );

            JODataProvider dataProvider = new JODataProvider( JamOrganizer.Properties.Settings.Default.DatabaseFilename );
            Window window = new MainWindow( new MainWindowViewModel( dataProvider ) );
            window.ShowDialog();
        }
    }
}
