﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JamOrganizer.Utilities
{
    /// <summary>
    /// Standard implementation of relay command shamelessly stolen from The Internet
    /// </summary>
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute = null;
        private readonly Predicate<T> _canExecute = null;

        public RelayCommand( Action<T> execute, Predicate<T> canExecute )
        {
            _execute = execute ?? throw new ArgumentNullException( "execute" );
            _canExecute = canExecute;
        }

        public RelayCommand( Action<T> execute ) : this( execute, null )
        {
        }

        public bool CanExecute( object parameter )
        {
            return _canExecute == null || _canExecute( (T) parameter );
        }

        public void Execute( object parameter )
        {
            _execute( (T) parameter );
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

    public class RelayCommand : RelayCommand<object>
    {
        public RelayCommand( Action<object> execute, Predicate<object> canExecute ) : base( execute, canExecute )
        {
        }

        public RelayCommand( Action<object> execute ) : base( execute )
        {
        }
    }
}
