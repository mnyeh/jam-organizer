#region Copyright Syncfusion Inc. 2001-2018.
// Copyright Syncfusion Inc. 2001-2018. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using Syncfusion.UI.Xaml.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFThemeStudio
{
    /// <summary>
    /// Interaction logic for Syncfusion_Controls.xaml
    /// </summary>
    public partial class Syncfusion_Controls : UserControl
    {
        public Syncfusion_Controls()
        {
            InitializeComponent();
            selectionmenu.Closed += selectionmenu_Closed;
            mainGrid.SizeChanged += mainGrid_SizeChanged;
            chart1.SizeChanged += chart1_SizeChanged;
            DateTime today = DateTime.Now;
            if (today.Month == 12)
            {
                today = today.AddMonths(-1);
            }
            else if (today.Month == 1)
            {
                today = today.AddMonths(1);
            }
            DateTime currentdate = new DateTime(today.Year, today.Month - 1, 1, 0, 0, 0);
            ScheduleAppointmentCollection AppointmentCollection = new ScheduleAppointmentCollection();

           
            Schedule.Appointments = AppointmentCollection;

            // Weekly Recursive Appointment

            ScheduleAppointment SchAppWeek = new ScheduleAppointment();
            SchAppWeek.Subject = "Doctor Appointment";
            SchAppWeek.Notes = "Weekly Recurrence";
            SchAppWeek.Location = "Global Hospital";
            SchAppWeek.StartTime = currentdate;
            SchAppWeek.EndTime = currentdate.AddHours(4);
            SchAppWeek.AppointmentBackground = new SolidColorBrush((Color.FromArgb(0xFF, 0xA2, 0xC1, 0x39)));

            // Setting Recurrence Properties

            RecurrenceProperties RecPropWeek = new RecurrenceProperties();
            RecPropWeek.RecurrenceType = RecurrenceType.Weekly;
            RecPropWeek.WeeklyEveryNWeeks = 1;
            RecPropWeek.IsWeeklyMonday = true;
            RecPropWeek.IsRangeRecurrenceCount = true;
            RecPropWeek.IsRangeNoEndDate = false;
            RecPropWeek.IsRangeEndDate = false;
            RecPropWeek.RangeRecurrenceCount = 4;

            // Generating RRule using ScheduleHelper

            SchAppWeek.RecurrenceRule = ScheduleHelper.RRuleGenerator(RecPropWeek, SchAppWeek.StartTime, SchAppWeek.EndTime);
            SchAppWeek.IsRecursive = true;
            AppointmentCollection.Add(SchAppWeek);

 

            ScheduleAppointment SchAppYear = new ScheduleAppointment();
            SchAppYear.Subject = "Wedding Anniversary";
            SchAppYear.Notes = "Yearly Recurrence";
            SchAppYear.Location = "Home";
            SchAppYear.StartTime = currentdate;
            SchAppYear.EndTime = currentdate.AddHours(4);
            SchAppYear.AppointmentBackground = new SolidColorBrush((Color.FromArgb(0xFF, 0x00, 0xAB, 0xA9)));

            // Setting Recurrence Properties using RRule

            SchAppYear.RecurrenceRule = "FREQ=YEARLY;COUNT=3;BYMONTHDAY=" + (DateTime.Now.Day).ToString() + ";BYMONTH=" + (DateTime.Now.Month).ToString() + "";
            SchAppYear.IsRecursive = true;
            AppointmentCollection.Add(SchAppYear);

            scrollViewer.Loaded += scrollViewer_Loaded;
        }

        void mainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            subGrid.Width = e.NewSize.Width - 30;
            Schedule.Width = e.NewSize.Width - 60;
        }

        void selectionmenu_Closed(object sender, RoutedEventArgs e)
        {
            selectionmenu.IsOpen = true;
        }

        void chart1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            datagrid1.Width = e.NewSize.Width;
        }

        void scrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            scrollViewer.ScrollChanged -= scrollViewer_ScrollChanged;
            scrollViewer.ScrollChanged += scrollViewer_ScrollChanged;
        }

        void scrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if(!mainGrid.IsMouseOver && !scrollViewer.IsMouseOver)
                scrollViewer.ScrollToHome();
        }

        private void Day_OnClick(object sender, RoutedEventArgs e)
        {
            switch ((sender as RadioButton).Name)
            {
                case "Day":
                    {
                        Schedule.ScheduleType = ScheduleType.Day;
                        break;
                    }
                case "Week":
                    {
                        Schedule.ScheduleType = ScheduleType.Week;
                        break;
                    }
                case "WorkWeek":
                    {
                        Schedule.ScheduleType = ScheduleType.WorkWeek;
                        break;
                    }
                case "Month":
                    {
                        Schedule.ScheduleType = ScheduleType.Month;
                        break;
                    }
                case "TimeLine":
                    {
                        Schedule.ScheduleType = ScheduleType.TimeLine;
                        break;
                    }
            }

        }
    }
}
