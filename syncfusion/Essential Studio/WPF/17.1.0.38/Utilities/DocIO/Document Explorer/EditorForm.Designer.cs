#region Copyright Syncfusion Inc. 2001-2019.
// Copyright Syncfusion Inc. 2001-2019. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Syncfusion.DocIOExplorer
{
  partial class EditorForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
      if( disposing && ( components != null ) )
      {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorForm));
        this.menuStrip = new System.Windows.Forms.MenuStrip();
        this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.closeDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        this.exitStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.documentExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.documentViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.documentLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.manualasPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.mainTreeView = new System.Windows.Forms.TreeView();
        this.treeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.removeNodeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        this.panelContent = new System.Windows.Forms.Panel();
        this.panel1 = new System.Windows.Forms.Panel();
        this.mainTextArea = new System.Windows.Forms.TextBox();
        this.label12 = new System.Windows.Forms.Label();
        this.panel4 = new System.Windows.Forms.Panel();
        this.label3 = new System.Windows.Forms.Label();
        this.label11 = new System.Windows.Forms.Label();
        this.label9 = new System.Windows.Forms.Label();
        this.panel3 = new System.Windows.Forms.Panel();
        this.richTextBox2 = new System.Windows.Forms.RichTextBox();
        this.label8 = new System.Windows.Forms.Label();
        this.panel2 = new System.Windows.Forms.Panel();
        this.richTextBox1 = new System.Windows.Forms.RichTextBox();
        this.panel5 = new System.Windows.Forms.Panel();
        this.button2 = new System.Windows.Forms.Button();
        this.label2 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.splitter1 = new System.Windows.Forms.Splitter();
        this.panelDocumentTree = new System.Windows.Forms.Panel();
        this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        this.expandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.collapseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.label7 = new System.Windows.Forms.Label();
        this.label1 = new System.Windows.Forms.Label();
        this.labelLine2 = new System.Windows.Forms.Label();
        this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
        this.imageList = new System.Windows.Forms.ImageList(this.components);
        this.panelHeader = new System.Windows.Forms.Panel();
        this.pictureBox2 = new System.Windows.Forms.PictureBox();
        this.pictureBox1 = new System.Windows.Forms.PictureBox();
        this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.menuStrip.SuspendLayout();
        this.treeContextMenu.SuspendLayout();
        this.panelContent.SuspendLayout();
        this.panel1.SuspendLayout();
        this.panel4.SuspendLayout();
        this.panel3.SuspendLayout();
        this.panel2.SuspendLayout();
        this.panel5.SuspendLayout();
        this.panelDocumentTree.SuspendLayout();
        this.menuStrip1.SuspendLayout();
        this.panelHeader.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
        this.SuspendLayout();
        // 
        // menuStrip
        // 
        this.menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
        this.menuStrip.Location = new System.Drawing.Point(0, 0);
        this.menuStrip.Name = "menuStrip";
        this.menuStrip.Size = new System.Drawing.Size(940, 24);
        this.menuStrip.TabIndex = 0;
        this.menuStrip.Text = "Menu";
        // 
        // fileToolStripMenuItem
        // 
        this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.closeDocumentToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitStripMenuItem});
        this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
        this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
        this.fileToolStripMenuItem.Text = "File";
        // 
        // openToolStripMenuItem
        // 
        this.openToolStripMenuItem.Name = "openToolStripMenuItem";
        this.openToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
        this.openToolStripMenuItem.Text = "Open";
        this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
        // 
        // saveAsToolStripMenuItem
        // 
        this.saveAsToolStripMenuItem.Enabled = false;
        this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
        this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
        this.saveAsToolStripMenuItem.Text = "Save As...";
        this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
        // 
        // closeDocumentToolStripMenuItem
        // 
        this.closeDocumentToolStripMenuItem.Enabled = false;
        this.closeDocumentToolStripMenuItem.Name = "closeDocumentToolStripMenuItem";
        this.closeDocumentToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
        this.closeDocumentToolStripMenuItem.Text = "Close Document";
        this.closeDocumentToolStripMenuItem.Click += new System.EventHandler(this.closeDocumentToolStripMenuItem_Click);
        // 
        // toolStripSeparator1
        // 
        this.toolStripSeparator1.Name = "toolStripSeparator1";
        this.toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
        // 
        // exitStripMenuItem
        // 
        this.exitStripMenuItem.Name = "exitStripMenuItem";
        this.exitStripMenuItem.Size = new System.Drawing.Size(162, 22);
        this.exitStripMenuItem.Text = "Exit";
        this.exitStripMenuItem.Click += new System.EventHandler(this.exitStripMenuItem_Click);
        // 
        // viewToolStripMenuItem
        // 
        this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentExplorerToolStripMenuItem,
            this.documentViewerToolStripMenuItem,
            this.documentLogToolStripMenuItem});
        this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
        this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
        this.viewToolStripMenuItem.Text = "View";
        // 
        // documentExplorerToolStripMenuItem
        // 
        this.documentExplorerToolStripMenuItem.Name = "documentExplorerToolStripMenuItem";
        this.documentExplorerToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
        this.documentExplorerToolStripMenuItem.Text = "Document Entities";
        this.documentExplorerToolStripMenuItem.Click += new System.EventHandler(this.documentExplorerToolStripMenuItem_Click);
        // 
        // documentViewerToolStripMenuItem
        // 
        this.documentViewerToolStripMenuItem.Name = "documentViewerToolStripMenuItem";
        this.documentViewerToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
        this.documentViewerToolStripMenuItem.Text = "Document Viewer";
        this.documentViewerToolStripMenuItem.Click += new System.EventHandler(this.documentViewerToolStripMenuItem_Click);
        // 
        // documentLogToolStripMenuItem
        // 
        this.documentLogToolStripMenuItem.Name = "documentLogToolStripMenuItem";
        this.documentLogToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
        this.documentLogToolStripMenuItem.Text = "Document Log";
        this.documentLogToolStripMenuItem.Click += new System.EventHandler(this.documentLogToolStripMenuItem_Click);
        // 
        // helpToolStripMenuItem
        // 
        this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.viewHelpToolStripMenuItem,
            this.manualasPdfToolStripMenuItem});
        this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
        this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
        this.helpToolStripMenuItem.Text = "Help";
        // 
        // aboutToolStripMenuItem
        // 
        this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
        this.aboutToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
        this.aboutToolStripMenuItem.Text = "About";
        this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
        // 
        // manualasPdfToolStripMenuItem
        // 
        this.manualasPdfToolStripMenuItem.Name = "manualasPdfToolStripMenuItem";
        this.manualasPdfToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
        this.manualasPdfToolStripMenuItem.Text = "Manual (as pdf)";
        this.manualasPdfToolStripMenuItem.Click += new System.EventHandler(this.manualasPdfToolStripMenuItem_Click);
        // 
        // mainTreeView
        // 
        this.mainTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.mainTreeView.ContextMenuStrip = this.treeContextMenu;
        this.mainTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mainTreeView.Location = new System.Drawing.Point(0, 78);
        this.mainTreeView.Name = "mainTreeView";
        this.mainTreeView.Size = new System.Drawing.Size(210, 438);
        this.mainTreeView.TabIndex = 0;
        this.mainTreeView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mainTreeView_MouseClick);
        this.mainTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.mainTreeView_AfterSelect);
        // 
        // treeContextMenu
        // 
        this.treeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeNodeToolStripMenuItem1});
        this.treeContextMenu.Name = "treeContextMenu";
        this.treeContextMenu.Size = new System.Drawing.Size(148, 26);
        this.treeContextMenu.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.treeContextMenu_Closed);
        this.treeContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.treeContextMenu_Opening);
        // 
        // removeNodeToolStripMenuItem1
        // 
        this.removeNodeToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("removeNodeToolStripMenuItem1.Image")));
        this.removeNodeToolStripMenuItem1.Name = "removeNodeToolStripMenuItem1";
        this.removeNodeToolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
        this.removeNodeToolStripMenuItem1.Text = "Remove node";
        this.removeNodeToolStripMenuItem1.Click += new System.EventHandler(this.removeEntity_Click);
        // 
        // panelContent
        // 
        this.panelContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.panelContent.Controls.Add(this.panel1);
        this.panelContent.Controls.Add(this.panel4);
        this.panelContent.Controls.Add(this.panel3);
        this.panelContent.Controls.Add(this.panel2);
        this.panelContent.Controls.Add(this.label4);
        this.panelContent.Controls.Add(this.splitter1);
        this.panelContent.Controls.Add(this.panelDocumentTree);
        this.panelContent.Controls.Add(this.labelLine2);
        this.panelContent.Controls.Add(this.menuStrip);
        this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
        this.panelContent.Location = new System.Drawing.Point(0, 58);
        this.panelContent.Name = "panelContent";
        this.panelContent.Size = new System.Drawing.Size(940, 541);
        this.panelContent.TabIndex = 1;
        // 
        // panel1
        // 
        this.panel1.Controls.Add(this.mainTextArea);
        this.panel1.Controls.Add(this.label12);
        this.panel1.Location = new System.Drawing.Point(233, 121);
        this.panel1.Name = "panel1";
        this.panel1.Size = new System.Drawing.Size(200, 384);
        this.panel1.TabIndex = 67;
        // 
        // mainTextArea
        // 
        this.mainTextArea.BackColor = System.Drawing.SystemColors.Window;
        this.mainTextArea.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mainTextArea.Location = new System.Drawing.Point(0, 27);
        this.mainTextArea.Multiline = true;
        this.mainTextArea.Name = "mainTextArea";
        this.mainTextArea.ReadOnly = true;
        this.mainTextArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.mainTextArea.Size = new System.Drawing.Size(200, 357);
        this.mainTextArea.TabIndex = 0;
        // 
        // label12
        // 
        this.label12.BackColor = System.Drawing.Color.Transparent;
        this.label12.Dock = System.Windows.Forms.DockStyle.Top;
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label12.Location = new System.Drawing.Point(0, 0);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(200, 27);
        this.label12.TabIndex = 61;
        this.label12.Text = "File Name:";
        this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // panel4
        // 
        this.panel4.BackColor = System.Drawing.Color.White;
        this.panel4.Controls.Add(this.label3);
        this.panel4.Controls.Add(this.label11);
        this.panel4.Controls.Add(this.label9);
        this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
        this.panel4.Location = new System.Drawing.Point(211, 52);
        this.panel4.Name = "panel4";
        this.panel4.Size = new System.Drawing.Size(729, 24);
        this.panel4.TabIndex = 70;
        // 
        // label3
        // 
        this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label3.Location = new System.Drawing.Point(5, 5);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(117, 20);
        this.label3.TabIndex = 61;
        this.label3.Text = "Document Entities";
        this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label3.Click += new System.EventHandler(this.label3_Click);
        // 
        // label11
        // 
        this.label11.BackColor = System.Drawing.Color.Transparent;
        this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label11.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label11.Location = new System.Drawing.Point(258, 6);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(117, 19);
        this.label11.TabIndex = 63;
        this.label11.Text = "Document Log";
        this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label11.Click += new System.EventHandler(this.label11_Click);
        // 
        // label9
        // 
        this.label9.BackColor = System.Drawing.Color.Transparent;
        this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label9.Location = new System.Drawing.Point(130, 6);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(117, 19);
        this.label9.TabIndex = 62;
        this.label9.Text = "Document Viewer";
        this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label9.Click += new System.EventHandler(this.label9_Click);
        // 
        // panel3
        // 
        this.panel3.BackColor = System.Drawing.Color.Transparent;
        this.panel3.Controls.Add(this.richTextBox2);
        this.panel3.Controls.Add(this.label8);
        this.panel3.Location = new System.Drawing.Point(458, 121);
        this.panel3.Name = "panel3";
        this.panel3.Size = new System.Drawing.Size(200, 372);
        this.panel3.TabIndex = 69;
        // 
        // richTextBox2
        // 
        this.richTextBox2.BackColor = System.Drawing.Color.White;
        this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
        this.richTextBox2.Location = new System.Drawing.Point(0, 27);
        this.richTextBox2.Name = "richTextBox2";
        this.richTextBox2.ReadOnly = true;
        this.richTextBox2.Size = new System.Drawing.Size(200, 345);
        this.richTextBox2.TabIndex = 71;
        this.richTextBox2.Text = "";
        // 
        // label8
        // 
        this.label8.BackColor = System.Drawing.Color.Transparent;
        this.label8.Dock = System.Windows.Forms.DockStyle.Top;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label8.Location = new System.Drawing.Point(0, 0);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(200, 27);
        this.label8.TabIndex = 72;
        this.label8.Text = "Lists the Document changes";
        this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // panel2
        // 
        this.panel2.BackColor = System.Drawing.Color.Transparent;
        this.panel2.Controls.Add(this.richTextBox1);
        this.panel2.Controls.Add(this.panel5);
        this.panel2.Location = new System.Drawing.Point(684, 121);
        this.panel2.Name = "panel2";
        this.panel2.Size = new System.Drawing.Size(272, 372);
        this.panel2.TabIndex = 68;
        // 
        // richTextBox1
        // 
        this.richTextBox1.BackColor = System.Drawing.SystemColors.Window;
        this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.richTextBox1.Location = new System.Drawing.Point(0, 41);
        this.richTextBox1.Name = "richTextBox1";
        this.richTextBox1.ReadOnly = true;
        this.richTextBox1.Size = new System.Drawing.Size(272, 331);
        this.richTextBox1.TabIndex = 1;
        this.richTextBox1.Text = "";
        // 
        // panel5
        // 
        this.panel5.Controls.Add(this.button2);
        this.panel5.Controls.Add(this.label2);
        this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
        this.panel5.Location = new System.Drawing.Point(0, 0);
        this.panel5.Name = "panel5";
        this.panel5.Size = new System.Drawing.Size(272, 41);
        this.panel5.TabIndex = 60;
        // 
        // button2
        // 
        this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
        this.button2.BackColor = System.Drawing.Color.Transparent;
        this.button2.Enabled = false;
        this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
        this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
        this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
        this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
        this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.button2.Location = new System.Drawing.Point(172, 10);
        this.button2.Name = "button2";
        this.button2.Size = new System.Drawing.Size(88, 24);
        this.button2.TabIndex = 57;
        this.button2.Text = "Open DOC";
        this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.button2.UseVisualStyleBackColor = false;
        this.button2.Click += new System.EventHandler(this.button2_Click);
        // 
        // label2
        // 
        this.label2.Dock = System.Windows.Forms.DockStyle.Left;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label2.Location = new System.Drawing.Point(0, 0);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(132, 41);
        this.label2.TabIndex = 59;
        this.label2.Text = "Document Preview";
        this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // label4
        // 
        this.label4.BackColor = System.Drawing.Color.Transparent;
        this.label4.Dock = System.Windows.Forms.DockStyle.Top;
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
        this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label4.Location = new System.Drawing.Point(211, 25);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(729, 27);
        this.label4.TabIndex = 60;
        this.label4.Text = "      Document Explorer";
        this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // splitter1
        // 
        this.splitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(172)))), ((int)(((byte)(178)))));
        this.splitter1.Location = new System.Drawing.Point(210, 25);
        this.splitter1.Name = "splitter1";
        this.splitter1.Size = new System.Drawing.Size(1, 516);
        this.splitter1.TabIndex = 65;
        this.splitter1.TabStop = false;
        // 
        // panelDocumentTree
        // 
        this.panelDocumentTree.Controls.Add(this.mainTreeView);
        this.panelDocumentTree.Controls.Add(this.menuStrip1);
        this.panelDocumentTree.Controls.Add(this.label7);
        this.panelDocumentTree.Controls.Add(this.label1);
        this.panelDocumentTree.Dock = System.Windows.Forms.DockStyle.Left;
        this.panelDocumentTree.Location = new System.Drawing.Point(0, 25);
        this.panelDocumentTree.Name = "panelDocumentTree";
        this.panelDocumentTree.Size = new System.Drawing.Size(210, 516);
        this.panelDocumentTree.TabIndex = 59;
        // 
        // menuStrip1
        // 
        this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.expandToolStripMenuItem,
            this.collapseToolStripMenuItem});
        this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
        this.menuStrip1.Location = new System.Drawing.Point(0, 54);
        this.menuStrip1.Name = "menuStrip1";
        this.menuStrip1.ShowItemToolTips = true;
        this.menuStrip1.Size = new System.Drawing.Size(210, 24);
        this.menuStrip1.TabIndex = 0;
        this.menuStrip1.Text = "menuStrip1";
        // 
        // expandToolStripMenuItem
        // 
        this.expandToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.expandToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("expandToolStripMenuItem.Image")));
        this.expandToolStripMenuItem.Name = "expandToolStripMenuItem";
        this.expandToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
        this.expandToolStripMenuItem.Text = "Expand";
        this.expandToolStripMenuItem.ToolTipText = "Expand All";
        this.expandToolStripMenuItem.Click += new System.EventHandler(this.expandAll_Click);
        // 
        // collapseToolStripMenuItem
        // 
        this.collapseToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.collapseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("collapseToolStripMenuItem.Image")));
        this.collapseToolStripMenuItem.Name = "collapseToolStripMenuItem";
        this.collapseToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
        this.collapseToolStripMenuItem.Text = "Collapse";
        this.collapseToolStripMenuItem.ToolTipText = "Collapse All";
        this.collapseToolStripMenuItem.Click += new System.EventHandler(this.collapseAll_Click);
        // 
        // label7
        // 
        this.label7.BackColor = System.Drawing.Color.White;
        this.label7.Dock = System.Windows.Forms.DockStyle.Top;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
        this.label7.ForeColor = System.Drawing.Color.Black;
        this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label7.Location = new System.Drawing.Point(0, 27);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(210, 27);
        this.label7.TabIndex = 59;
        this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // label1
        // 
        this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.label1.Dock = System.Windows.Forms.DockStyle.Top;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
        this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
        this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.label1.Location = new System.Drawing.Point(0, 0);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(210, 27);
        this.label1.TabIndex = 58;
        this.label1.Text = "      Document tree";
        this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // labelLine2
        // 
        this.labelLine2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(172)))), ((int)(((byte)(178)))));
        this.labelLine2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.labelLine2.Dock = System.Windows.Forms.DockStyle.Top;
        this.labelLine2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelLine2.ForeColor = System.Drawing.Color.MidnightBlue;
        this.labelLine2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.labelLine2.Location = new System.Drawing.Point(0, 24);
        this.labelLine2.Name = "labelLine2";
        this.labelLine2.Size = new System.Drawing.Size(940, 1);
        this.labelLine2.TabIndex = 66;
        this.labelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // openFileDialog
        // 
        this.openFileDialog.Filter = "Word documents (*.doc)|*.doc| All files (*.*) | *.*";
        this.openFileDialog.Title = "Opend Document";
        // 
        // saveFileDialog
        // 
        this.saveFileDialog.Filter = "Word Documents(*.doc) | *.doc | Text Documents (*.txt) | *.txt | XmlDocuments (*." +
            "xml) | *.xml | All files | *.*";
        this.saveFileDialog.Title = "Save Document As";
        // 
        // imageList
        // 
        this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
        this.imageList.TransparentColor = System.Drawing.Color.Transparent;
        this.imageList.Images.SetKeyName(0, "OrganizerHS.png");
        this.imageList.Images.SetKeyName(1, "section.png");
        this.imageList.Images.SetKeyName(2, "headerfooter.png");
        this.imageList.Images.SetKeyName(3, "table.png");
        this.imageList.Images.SetKeyName(4, "row.png");
        this.imageList.Images.SetKeyName(5, "cell.png");
        this.imageList.Images.SetKeyName(6, "app.png");
        this.imageList.Images.SetKeyName(7, "body.png");
        this.imageList.Images.SetKeyName(8, "bookmarkend.png");
        this.imageList.Images.SetKeyName(9, "bookmarkstart.png");
        this.imageList.Images.SetKeyName(10, "fieldend.gif");
        this.imageList.Images.SetKeyName(11, "fieldseperator.gif");
        this.imageList.Images.SetKeyName(12, "fieldstart.gif");
        this.imageList.Images.SetKeyName(13, "footer.png");
        this.imageList.Images.SetKeyName(14, "footnote.png");
        this.imageList.Images.SetKeyName(15, "formcheckbox.png");
        this.imageList.Images.SetKeyName(16, "formdropdown.png");
        this.imageList.Images.SetKeyName(17, "formfield.png");
        this.imageList.Images.SetKeyName(18, "formtextinput.png");
        this.imageList.Images.SetKeyName(19, "groupshape.png");
        this.imageList.Images.SetKeyName(20, "header.png");
        this.imageList.Images.SetKeyName(21, "inlineshape.png");
        this.imageList.Images.SetKeyName(22, "node.png");
        this.imageList.Images.SetKeyName(23, "paragraph.png");
        this.imageList.Images.SetKeyName(24, "txtrange.png");
        this.imageList.Images.SetKeyName(25, "document_into.png");
        this.imageList.Images.SetKeyName(26, "shape.png");
        this.imageList.Images.SetKeyName(27, "specialchar.png");
        this.imageList.Images.SetKeyName(28, "add_gray.png");
        this.imageList.Images.SetKeyName(29, "ArrangeWindowsHS.png");
        this.imageList.Images.SetKeyName(30, "DataContainer_MovePreviousHS.png");
        this.imageList.Images.SetKeyName(31, "DataContainer_MoveNextHS.png");
        this.imageList.Images.SetKeyName(32, "TableHS.png");
        this.imageList.Images.SetKeyName(33, "DataContainer_MoveLastHS.png");
        this.imageList.Images.SetKeyName(34, "ViewThumbnailsHS.png");
        this.imageList.Images.SetKeyName(35, "NewReportHS.png");
        this.imageList.Images.SetKeyName(36, "picture.png");
        this.imageList.Images.SetKeyName(37, "header.png");
        // 
        // panelHeader
        // 
        this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(239)))), ((int)(((byte)(245)))));
        this.panelHeader.Controls.Add(this.pictureBox2);
        this.panelHeader.Controls.Add(this.pictureBox1);
        this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
        this.panelHeader.Location = new System.Drawing.Point(0, 0);
        this.panelHeader.Name = "panelHeader";
        this.panelHeader.Size = new System.Drawing.Size(940, 58);
        this.panelHeader.TabIndex = 5;
        // 
        // pictureBox2
        // 
        this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
        this.pictureBox2.Image = global::Syncfusion.DocIOExplorer.Properties.Resources.header_image;
        this.pictureBox2.Location = new System.Drawing.Point(559, 0);
        this.pictureBox2.Name = "pictureBox2";
        this.pictureBox2.Size = new System.Drawing.Size(381, 58);
        this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
        this.pictureBox2.TabIndex = 29;
        this.pictureBox2.TabStop = false;
        // 
        // pictureBox1
        // 
        this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.pictureBox1.Image = global::Syncfusion.DocIOExplorer.Properties.Resources.DocIO_header1;
        this.pictureBox1.Location = new System.Drawing.Point(0, 0);
        this.pictureBox1.Name = "pictureBox1";
        this.pictureBox1.Size = new System.Drawing.Size(940, 58);
        this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
        this.pictureBox1.TabIndex = 28;
        this.pictureBox1.TabStop = false;
        // 
        // viewHelpToolStripMenuItem
        // 
        this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
        this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
        this.viewHelpToolStripMenuItem.Text = "View Help File";
        this.viewHelpToolStripMenuItem.Click += new System.EventHandler(this.viewHelpToolStripMenuItem_Click);
        // 
        // EditorForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.SystemColors.Control;
        this.ClientSize = new System.Drawing.Size(940, 599);
        this.Controls.Add(this.panelContent);
        this.Controls.Add(this.panelHeader);
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStrip;
        this.MinimumSize = new System.Drawing.Size(600, 120);
        this.Name = "EditorForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Essential DocIO - Document Explorer";
        this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        this.menuStrip.ResumeLayout(false);
        this.menuStrip.PerformLayout();
        this.treeContextMenu.ResumeLayout(false);
        this.panelContent.ResumeLayout(false);
        this.panelContent.PerformLayout();
        this.panel1.ResumeLayout(false);
        this.panel1.PerformLayout();
        this.panel4.ResumeLayout(false);
        this.panel3.ResumeLayout(false);
        this.panel2.ResumeLayout(false);
        this.panel5.ResumeLayout(false);
        this.panelDocumentTree.ResumeLayout(false);
        this.panelDocumentTree.PerformLayout();
        this.menuStrip1.ResumeLayout(false);
        this.menuStrip1.PerformLayout();
        this.panelHeader.ResumeLayout(false);
        this.panelHeader.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
      private System.Windows.Forms.ToolStripMenuItem exitStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.TreeView mainTreeView;
    private System.Windows.Forms.TextBox mainTextArea;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.SaveFileDialog saveFileDialog;
    private System.Windows.Forms.ContextMenuStrip treeContextMenu;
      private System.Windows.Forms.ToolStripMenuItem removeNodeToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
      private System.Windows.Forms.RichTextBox richTextBox1;
      private System.Windows.Forms.Panel panelContent;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem expandToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem collapseToolStripMenuItem;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button button2;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Panel panel5;
      private System.Windows.Forms.Splitter splitter1;
      private System.Windows.Forms.Panel panelDocumentTree;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label labelLine2;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Panel panel4;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.ToolStripMenuItem closeDocumentToolStripMenuItem;
      private System.Windows.Forms.RichTextBox richTextBox2;
      private System.Windows.Forms.ToolStripMenuItem documentExplorerToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem documentViewerToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem documentLogToolStripMenuItem;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.ImageList imageList;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Panel panelHeader;
      private System.Windows.Forms.PictureBox pictureBox2;
      private System.Windows.Forms.ToolStripMenuItem manualasPdfToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
  }
}

