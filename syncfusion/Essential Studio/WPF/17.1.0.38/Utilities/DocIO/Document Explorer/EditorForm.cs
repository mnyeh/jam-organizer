#region Copyright Syncfusion Inc. 2001-2019.
// Copyright Syncfusion Inc. 2001-2019. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIO;
using Syncfusion.DocIOExplorer;
using System.Diagnostics;

namespace Syncfusion.DocIOExplorer
{
    public partial class EditorForm : Form
    {
        #region Fields
        private WordDocument m_doc;
        private TreeNode m_contextNode = null;

        #endregion

        #region Constructor
        public EditorForm()
        {
            InitializeComponent();
            showExplorer();
        }
        #endregion

        #region Event handlers

        #region Expand
        /// <summary>
        /// Handles the Click event of the expandAllToolStripMenuItem control.
        /// </summary>
        private void expandAll_Click(object sender, EventArgs e)
        {
            mainTreeView.ExpandAll();
        }
        #endregion

        #region Collapse
        /// <summary>
        /// Handles the Click event of the collapseAllToolStripMenuItem control.
        /// </summary>
        private void collapseAll_Click(object sender, EventArgs e)
        {
            mainTreeView.CollapseAll();
        }
        #endregion

        #region Remove Entity
        /// <summary>
        /// Handles the Click event of the removeEntity_EditMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void removeEntity_Click(object sender, EventArgs e)
        {
            this.ActiveControl = this.mainTreeView;
            Entity ent = mainTreeView.SelectedNode.Tag as Entity;
            if (ent.EntityType == EntityType.WordDocument)
            {
                MessageBox.Show("You cannot remove root node");
            }
            else if (ent.EntityType == EntityType.TextBody)
            {
                MessageBox.Show("You cannot remove TextBody - it is property");
            }
            else
            {
                Entity owner = ent.Owner;
                if (owner != null && owner.IsComposite)
                {
                    (owner as ICompositeEntity).ChildEntities.Remove(ent);
                    mainTreeView.SelectedNode.Remove();
                    FillLogTextBox("Removed " + ent.GetType().ToString().Substring(22));
                }
            }
        }
        #endregion

        #region Main Tree AfterSelecct

        /// <summary>
        /// Handles the AfterSelect event of the mainTreeView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.TreeViewEventArgs"/> instance containing the event data.</param>
        private void mainTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            Entity ent = e.Node.Tag as Entity;
            if (ent != null)
            {
                mainTextArea.Text = GetText(ent);
                this.label7.Text = ent.ToString();
            }

        }
        #endregion

        #region Open Document

        /// <summary>
        /// Handles the Click event of the openToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        String filepath;

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = Application.StartupPath + @"\Instruction";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    Stream stream = openFileDialog.OpenFile();
                    m_doc = new WordDocument(stream);
                    this.label12.Text = "Document: " + openFileDialog.FileName;
                    
                    #region Save a local copy

                    filepath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "..\\Sample.doc";
                    //Char[] ch = new Char[] { '.', 'E', 'X', 'E' };
                    //filepath = filepath.TrimEnd(ch) + ".doc";
                    m_doc.Save(filepath);

                    #endregion
                    richTextBox1.Text = m_doc.GetText();
                    FillDocumentTree();
                    this.mainTreeView.SelectedNode = this.mainTreeView.Nodes[0];
                }
                catch (Exception ex)
                {
                    string message = ex.Message + "\r\n" + ex.StackTrace;
                    MessageBox.Show(message, "Exception during document opening",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    m_doc = null;
                    mainTreeView.Nodes.Clear();
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            //Message box confirmation to view the created document.
            if (MessageBox.Show("Do you want to view the MS Word document?", "Document has been created", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                //Launching the MS Word file using the default Application.[MS Word Or Free WordViewer]
                System.Diagnostics.Process.Start(filepath);
            }
        }
        #endregion

        #region Save Document
        /// <summary>
        /// Handles the Click event of the saveAsToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        string newFilePath;
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                Stream stream = saveFileDialog.OpenFile();
                FormatType saveType = FormatType.Doc;

                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        saveType = FormatType.Doc;
                        break;
                    case 2:
                        saveType = FormatType.Txt;
                        break;
                    case 3:
                        saveType = FormatType.Xml;
                        break;
                }

                m_doc.Save(stream, saveType);
                m_doc.Save(filepath);
                richTextBox1.Text = m_doc.GetText();

            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveAsToolStripMenuItem_Click(sender, EventArgs.Empty);
        }
        #endregion

        #region About

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About obj = new About();
            obj.ShowDialog();
        }
        #endregion

        #region Close Document
        private void closeDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainTreeView.Nodes.Clear();
            mainTextArea.Text = "";
            this.label7.Text = "";
            this.label12.Text = "File Name";
            this.richTextBox2.Clear();
            this.richTextBox1.Clear();
            this.button2.Enabled = false;
            this.closeDocumentToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Enabled = false;
            m_doc = null;
        }
        #endregion

        #region Exit
        /// <summary>
        /// Handles the Click event of the exitStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void exitStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region MainTreeView MouseClick
        /// <summary>
        /// Handles the MouseClick event of the mainTreeView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
        private void mainTreeView_MouseClick(object sender, MouseEventArgs e)
        {
            removeNodeToolStripMenuItem1.Enabled = true;
            m_contextNode = mainTreeView.GetNodeAt(e.Location);
        }
        #endregion

        #region Open Tree Context Menu
        /// <summary>
        /// Handles the Opening event of the treeContextMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void treeContextMenu_Opening(object sender, CancelEventArgs e)
        {
            mainTreeView.SelectedNode = m_contextNode;

            Entity ent = m_contextNode.Tag as Entity;
            ToolStripItem tsItem = null;

            switch (ent.EntityType)
            {
                case EntityType.WordDocument:
                    treeContextMenu.Items.Add("-");
                    tsItem = treeContextMenu.Items.Add("Add section");
                    tsItem.Click += new EventHandler(tsItemAddSection_Click);
                    break;
                case EntityType.Section:
                    treeContextMenu.Items.Add("-");
                    tsItem = treeContextMenu.Items.Add("Insert section");
                    tsItem.Click += new EventHandler(tsItemAddSection_Click);
                    break;
                case EntityType.TextBody:
                    treeContextMenu.Items.Add("-");
                    tsItem = treeContextMenu.Items.Add("Add paragraph");
                    tsItem.Click += new EventHandler(tsItemAddParagraph_Click);
                    break;
                case EntityType.Paragraph:
                    treeContextMenu.Items.Add("-");
                    tsItem = treeContextMenu.Items.Add("Add text");
                    tsItem.Click += new EventHandler(tsItemAddText_Click);
                    tsItem = treeContextMenu.Items.Add("Insert paragraph");
                    tsItem.Click += new EventHandler(tsItemAddParagraph_Click);
                    break;
                default:
                    if (ent is ParagraphItem)
                    {
                        treeContextMenu.Items.Add("-");
                        tsItem = treeContextMenu.Items.Add("Insert text");
                        tsItem.Click += new EventHandler(tsItemAddText_Click);
                    }
                    break;
            }

        }
        #endregion

        #region Insert

        #region Insert Section
        /// <summary>
        /// Handles the Click event of the tsItemAddSection control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void tsItemAddSection_Click(object sender, EventArgs e)
        {
            if (m_contextNode != null)
            {
                WordDocument doc = m_contextNode.Tag as WordDocument;
                IWSection sec = null;
                TreeNode node = null;
                if (doc != null)
                {
                    sec = doc.AddSection();
                    Entity en = sec as Entity;
                    node = m_contextNode.Nodes.Add(sec.EntityType.ToString());
                    assignImages(en, node);
                }
                else
                {
                    IEntity owner = m_contextNode.Tag as IEntity;
                    //sec = ( IWSection )InsertBefore( owner, new WSection( owner.Document ) );
                    sec = (IWSection)InsertAfter(owner, new WSection(owner.Document));
                    //node = InsertNodeBefore( m_contextNode, sec.EntityType.ToString() );
                    node = InsertNodeAfter(m_contextNode, sec.EntityType.ToString());
                    assignImages(new WSection(owner.Document), node);
                }
                node.Tag = sec;
                // TextBody node
                node = node.Nodes.Add(sec.Body.EntityType.ToString());
                Entity enText = sec.Body as Entity;
                FillLogTextBox("Added Section");
                node.Tag = sec.Body;
                assignImages(enText, node);
                IWParagraph para = sec.AddParagraph();
                Entity enPara = para as Entity;
                node = node.Nodes.Add(para.EntityType.ToString());
                node.Tag = para;
                assignImages(enPara, node);

            }
        }
        #endregion

        #region Insert Paragraph
        /// <summary>
        /// Handles the Click event of the tsItemAddParagraph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void tsItemAddParagraph_Click(object sender, EventArgs e)
        {
            WSection sec = null;
            if (m_contextNode.Tag is WTextBody)
            {
                sec = (m_contextNode.Tag as WTextBody).Owner as WSection;
            }
            else
            {
                sec = m_contextNode.Tag as WSection;
            }

            IWParagraph para = null;
            TreeNode node = null;
            if (sec != null)
            {
                para = sec.AddParagraph();
                Entity en = para as Entity;
                node = m_contextNode.Nodes.Add(para.EntityType.ToString());
                assignImages(en, node);
            }
            else
            {
                IEntity owner = m_contextNode.Tag as IEntity;
                //para = ( IWParagraph )InsertBefore( owner, new WParagraph( owner.Document ) );
                para = (IWParagraph)InsertAfter(owner, new WParagraph(owner.Document));
                //node = InsertNodeBefore( m_contextNode, para.EntityType.ToString() );
                node = InsertNodeAfter(m_contextNode, para.EntityType.ToString());
                assignImages(new WParagraph(owner.Document), node);
            }
            FillLogTextBox("Added Paragraph");
            node.Tag = para;
            //if (newEntity != null)
            //  assignImages(newEntity, node);
        }

        #endregion

        #region Insert Text
        /// <summary>
        /// Handles the Click event of the tsItemAddParagraph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void tsItemAddText_Click(object sender, EventArgs e)
        {
            AddTextRangeForm textRangeForm = new AddTextRangeForm();
            DialogResult result = textRangeForm.ShowDialog();

            if (result == DialogResult.OK)
            {
                WParagraph para = m_contextNode.Tag as WParagraph;
                IWTextRange tr = null;
                TreeNode node = null;
                if (para != null)
                {
                    tr = para.AppendText(textRangeForm.InputText);
                    Entity en = tr as Entity;
                    node = m_contextNode.Nodes.Add(tr.EntityType.ToString());
                    assignImages(en, node);
                }
                else
                {
                    IEntity owner = m_contextNode.Tag as IEntity;
                    //tr = ( IWTextRange )InsertBefore( owner, new WTextRange( owner.Document ) );
                    tr = (IWTextRange)InsertAfter(owner, new WTextRange(owner.Document));
                    if (owner is WTextRange)
                    {
                        tr.ApplyCharacterFormat((owner as WTextRange).CharacterFormat);
                    }
                    tr.Text = textRangeForm.InputText;
                    //node = InsertNodeBefore( m_contextNode, tr.EntityType.ToString() );
                    node = InsertNodeAfter(m_contextNode, tr.EntityType.ToString());
                    assignImages(new WTextRange(owner.Document), node);
                }
                FillLogTextBox("Added Text");
                node.Tag = tr;


            }

        }
        #endregion

        #region Insert Entity Method
        /// <summary>
        /// Inserts the before.
        /// </summary>
        /// <param name="selEnt">The sel ent.</param>
        /// <param name="newEnt">The new ent.</param>
        /// <returns></returns>
        private IEntity InsertBefore(IEntity selEnt, Entity newEnt)
        {
            ICompositeEntity ownerEnt = (selEnt.Owner as ICompositeEntity);
            int index = ownerEnt.ChildEntities.IndexOf(selEnt);
            ownerEnt.ChildEntities.Insert(index, newEnt);
            return newEnt;

        }

        /// <summary>
        /// Inserts entity after selected one.
        /// </summary>
        /// <param name="selEnt"></param>
        /// <param name="newEnt"></param>
        /// <returns></returns>

        private IEntity InsertAfter(IEntity selEnt, Entity newEnt)
        {
            ICompositeEntity ownerEnt = (selEnt.Owner as ICompositeEntity);
            ownerEnt.ChildEntities.Add(newEnt);
            return newEnt;
        }

        #endregion

        #region Insert Node in TreeView
        /// <summary>
        /// Inserts the node before.
        /// </summary>
        /// <param name="selNode">The selected node.</param>
        /// <param name="nodeName">Name of the node.</param>
        /// <returns></returns>
        private TreeNode InsertNodeBefore(TreeNode selNode, string nodeName)
        {
            TreeNode ownerNode = m_contextNode.Parent;
            int index = ownerNode.Nodes.IndexOf(selNode);
            return ownerNode.Nodes.Insert(index, nodeName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selNode"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        private TreeNode InsertNodeAfter(TreeNode selNode, string nodeName)
        {
            TreeNode ownerNode = m_contextNode.Parent;

            return ownerNode.Nodes.Add(nodeName);

        }

        #endregion
        #endregion

        # region Help
        /// <summary>
        /// Loads the help file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Application.StartupPath + @"\Instruction\Document Explorer Utility Help File.chm");
        }
        # endregion

        /// <summary>
        /// Handles the Closed event of the treeContextMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.ToolStripDropDownClosedEventArgs"/> instance containing the event data.</param>
        private void treeContextMenu_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            while (treeContextMenu.Items.Count > 1)
            {
                treeContextMenu.Items.RemoveAt(1);
            }
        }

        //Help menu with the manual as pdf.
        private void manualasPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Application.StartupPath + @"\Instruction\Document Explorer Instruction Manual.pdf");
            }
            catch (Win32Exception ex)
            {
                MessageBox.Show("File cannot be opened.");
            }
        }

        #endregion

        #region Helper methods

        #region FillDocumentTree
        /// <summary>
        /// Fills the document tree.
        /// </summary>
        private void FillDocumentTree()
        {
            mainTreeView.Nodes.Clear();
            mainTextArea.Text = "";
            saveAsToolStripMenuItem.Enabled = true;
            mainTreeView.ImageList = imageList;
            TreeNode node = mainTreeView.Nodes.Add("WordDocument");
            node.Tag = m_doc;
            node.ImageIndex = 1;
            node.SelectedImageIndex = 1;
            EnumerateChildren(m_doc, node);
            this.button2.Enabled = true;
            this.closeDocumentToolStripMenuItem.Enabled = true;
        }
        #region EnumerateChildren
        /// <summary>
        /// Enumerates the children.
        /// </summary>
        /// <param name="m_doc">The m_doc.</param>
        /// <param name="node">The node.</param>
        private void EnumerateChildren(ICompositeEntity compEntity, TreeNode node)
        {
            foreach (Entity entity in compEntity.ChildEntities)
            {
                HandleEntity(entity, node.Nodes);
            }
        }
        #endregion
        #endregion

        #region FillLogTextBox
        private void FillLogTextBox(string str)
        {
            this.richTextBox2.Text += str + "\n";
        }
        #endregion

        #region HandleEntity
        /// <summary>
        /// Handles the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="treeNodeCollection">The tree node collection.</param>
        private void HandleEntity(Entity entity, TreeNodeCollection nodes)
        {
            TreeNode node = null;

            switch (entity.EntityType)
            {
                case EntityType.TextRange:
                    node = nodes.Add("TextRange");
                    node.ImageIndex = 24;
                    node.SelectedImageIndex = 24;
                    break;
                case EntityType.Section:
                    node = nodes.Add(entity.EntityType.ToString());
                    node.ImageIndex = 25;
                    node.SelectedImageIndex = 25;
                    WSection sec = entity as WSection;
                    HandleHF(sec.HeadersFooters.Header, "Header", node.Nodes);
                    HandleHF(sec.HeadersFooters.Footer, "Footer", node.Nodes);
                    HandleHF(sec.HeadersFooters.EvenHeader, "EvenHeader", node.Nodes);
                    HandleHF(sec.HeadersFooters.EvenFooter, "EvenFooter", node.Nodes);
                    HandleHF(sec.HeadersFooters.FirstPageHeader, "FirstPageHeader", node.Nodes);
                    HandleHF(sec.HeadersFooters.FirstPageFooter, "FirstPageFooter", node.Nodes);
                    HandleEntity(sec.Body, node.Nodes);
                    break;
                case EntityType.BookmarkEnd:
                    string beName = (entity as BookmarkEnd).Name;
                    node = nodes.Add(entity.EntityType.ToString() + "-\"" + beName + "\"");
                    node.ImageIndex = 8;
                    node.SelectedImageIndex = 8;
                    break;
                case EntityType.BookmarkStart:
                    string bsName = (entity as BookmarkStart).Name;
                    node = nodes.Add(entity.EntityType.ToString() + "-\"" + bsName + "\"");
                    node.ImageIndex = 9;
                    node.SelectedImageIndex = 9;
                    break;
                case EntityType.TextFormField:
                    string ffName = (entity as WFormField).Name;
                    node = nodes.Add(entity.EntityType.ToString() + "-\"" + ffName + "\"");
                    break;
                case EntityType.FieldMark:
                    string fieldMarkType = ((entity as WFieldMark).Type == FieldMarkType.FieldSeparator) ? "Field Separator" : "FieldEnd";
                    node = nodes.Add(fieldMarkType);
                    if (fieldMarkType == "Field Separator")
                    {
                        node.ImageIndex = 11;
                        node.SelectedImageIndex = 11;
                    }
                    if (fieldMarkType == "FieldEnd")
                    {
                        node.ImageIndex = 10;
                        node.SelectedImageIndex = 10;
                    }
                    break;
                case EntityType.HeaderFooter:
                    if ((entity as HeaderFooter).ChildEntities.Count > 0)
                    {
                        node = nodes.Add(entity.EntityType.ToString());
                        node.ImageIndex = 2;
                        node.SelectedImageIndex = 2;
                    }
                    break;

                default:
                    node = nodes.Add(entity.EntityType.ToString());
                    assignImages(entity, node);

                    break;
            }

            if (node != null)
            {
                node.Tag = entity;
                if (entity.EntityType != EntityType.Section)
                {
                    ICompositeEntity compEntity = entity as ICompositeEntity;

                    if (compEntity != null)
                    {
                        EnumerateChildren(compEntity, node);
                    }
                }
            }
        }
        #endregion

        #region Assign Images
        private void assignImages(Entity entity, TreeNode node)
        {
            //Add icons for tree nodes
            if (EntityType.BookmarkEnd == entity.EntityType)
            {
                node.ImageIndex = 8;
                node.SelectedImageIndex = 8;
            }
            if (EntityType.BookmarkStart == entity.EntityType)
            {
                node.ImageIndex = 9;
                node.SelectedImageIndex = 9;
            }
            if (EntityType.Break == entity.EntityType)
            {
                node.ImageIndex = 29;
                node.SelectedImageIndex = 29;
            }
            if (EntityType.CheckBox == entity.EntityType)
            {
                node.ImageIndex = 15;
                node.SelectedImageIndex = 15;
            }
            if (EntityType.Comment == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }
            if (EntityType.DropDownFormField == entity.EntityType)
            {
                node.ImageIndex = 16;
                node.SelectedImageIndex = 16;
            }
            if (EntityType.EmbededField == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }
            if (EntityType.Field == entity.EntityType)
            {
                node.ImageIndex = 17;
                node.SelectedImageIndex = 17;
            }
            if (EntityType.FieldMark == entity.EntityType)
            {
                string fieldMarkType = ((entity as WFieldMark).Type == FieldMarkType.FieldSeparator) ? "Field Separator" : "FieldEnd";
                if (fieldMarkType == "Field Separator")
                {
                    node.ImageIndex = 11;
                    node.SelectedImageIndex = 11;
                }
                if (fieldMarkType == "FieldEnd")
                {
                    node.ImageIndex = 10;
                    node.SelectedImageIndex = 10;
                }
            }
            if (EntityType.Footnote == entity.EntityType)
            {
                node.ImageIndex = 14;
                node.SelectedImageIndex = 14;
            }
            if (EntityType.MergeField == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }
            if (EntityType.Paragraph == entity.EntityType)
            {
                node.ImageIndex = 23;
                node.SelectedImageIndex = 23;
            }
            if (EntityType.Picture == entity.EntityType)
            {
                node.ImageIndex = 36;
                node.SelectedImageIndex = 36;
            }
            if (EntityType.Section == entity.EntityType)
            {
                node.ImageIndex = 25;
                node.SelectedImageIndex = 25;
            }
            if (EntityType.SeqField == entity.EntityType)
            {

                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }

            if (EntityType.Shape == entity.EntityType)
            {
                node.ImageIndex = 26;
                node.SelectedImageIndex = 26;
            }
            if (EntityType.Symbol == entity.EntityType)
            {
                node.ImageIndex = 27;
                node.SelectedImageIndex = 27;
            }
            if (EntityType.TOC == entity.EntityType)
            {
                node.ImageIndex = 32;
                node.SelectedImageIndex = 32;
            }

            if (EntityType.Table == entity.EntityType)
            {
                node.ImageIndex = 3;
                node.SelectedImageIndex = 3;
            }

            if (EntityType.TableCell == entity.EntityType)
            {
                node.ImageIndex = 5;
                node.SelectedImageIndex = 5;
            }
            if (EntityType.TableRow == entity.EntityType)
            {
                node.ImageIndex = 4;
                node.SelectedImageIndex = 4;
            }
            if (EntityType.TextRange == entity.EntityType)
            {
                node.ImageIndex = 24;
                node.SelectedImageIndex = 24;
            }
            if (EntityType.TextBody == entity.EntityType)
            {
                node.ImageIndex = 7;
                node.SelectedImageIndex = 7;
            }

            if (EntityType.TextBox == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }

            if (EntityType.TextFormField == entity.EntityType)
            {
                node.ImageIndex = 18;
                node.SelectedImageIndex = 18;
            }

            if (EntityType.Undefined == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }
            if (EntityType.WordDocument == entity.EntityType)
            {
                node.ImageIndex = 1;
                node.SelectedImageIndex = 1;
            }

        }
        /// <summary>
        /// Handles the HeaderFooter.
        /// </summary>
        /// <param name="hf">The headerFooter.</param>
        /// <param name="nodeTitle">The node title.</param>
        /// <param name="nodes">The nodes.</param>
        private void HandleHF(HeaderFooter hf, string nodeTitle, TreeNodeCollection nodes)
        {
            if (hf.ChildEntities.Count > 0)
            {
                TreeNode node = nodes.Add("HeaderFooter - " + nodeTitle);
                node.Tag = hf;

                EnumerateChildren(hf, node);
                if (nodeTitle == "Header")
                {
                    node.ImageIndex = 37;
                    node.SelectedImageIndex = 37;
                }
                else if(nodeTitle == "Footer")
                {
                    node.ImageIndex = 13;
                    node.SelectedImageIndex = 13;
                }
            }
        }
        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        private string GetText(Entity entity)
        {
            StringBuilder sb = new StringBuilder();

            switch (entity.EntityType)
            {
                case EntityType.TextRange:
                    sb.Append((entity as WTextRange).Text);
                    break;
                case EntityType.WordDocument:
                case EntityType.TextBody:
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    break;
                case EntityType.Section:
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    sb.Append("{SectionBreak}");
                    break;
                case EntityType.HeaderFooter:
                    if ((entity as HeaderFooter).ChildEntities.Count > 0)
                    {
                        sb.Append("{HeaderFooterStart}");
                        sb.Append(GetChildrenText(entity as ICompositeEntity));
                        sb.Append("{HeaderFooterEnd}");
                    }
                    break;
                case EntityType.Table:
                    sb.Append("\r\n{TableStart}");
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    sb.Append("\r\n{TableEnd}");
                    break;
                case EntityType.TableCell:
                    sb.Append("{CellStart}");
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    sb.Append("{CellEnd}");
                    break;
                case EntityType.TableRow:
                    sb.Append("\r\n{RowStart}");
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    sb.Append("\r\n{RowEnd}");
                    break;
                case EntityType.Paragraph:
                    sb.Append(GetChildrenText(entity as ICompositeEntity));
                    sb.Append("�\r\n");
                    break;
                case EntityType.FieldMark:
                    WFieldMark fieldMark = entity as WFieldMark;
                    if (fieldMark.Type == FieldMarkType.FieldSeparator)
                        sb.Append("{FieldSeparator}");
                    else
                        sb.Append("{FieldEnd}");
                    break;
                case EntityType.CheckBox:
                    sb.Append("{FORMCHECKBOX}");
                    break;
                case EntityType.TextFormField:
                    sb.Append("{FORMTEXT}");
                    break;
                case EntityType.DropDownFormField:
                    sb.Append("{FORMDROPDOWN}");
                    break;

                default:
                    sb.Append("{");
                    sb.Append(entity.EntityType.ToString());
                    sb.Append("}");
                    break;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Expands all nodes under currently selected node.
        /// </summary>
        private void ExpandAll()
        {
            // This operation can take some time so we set the Cursor to WaitCursor.
            Application.DoEvents();
            Cursor cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (this.mainTreeView.SelectedNode != null)
            {
                this.mainTreeView.BeginUpdate();
                this.mainTreeView.SelectedNode.ExpandAll();
                this.mainTreeView.SelectedNode.EnsureVisible();
                this.mainTreeView.EndUpdate();
            }

            // Restore cursor.
            Cursor.Current = cursor;
        }
        /// <summary>
        /// Collapses all nodes under currently selected node.
        /// </summary>
        private void CollapseAll()
        {
            // This operation can take some time so we set the Cursor to WaitCursor.
            Application.DoEvents();
            Cursor cursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            if (this.mainTreeView.SelectedNode != null)
            {
                this.mainTreeView.BeginUpdate();
                this.mainTreeView.SelectedNode.Collapse();
                this.mainTreeView.SelectedNode.EnsureVisible();
                this.mainTreeView.EndUpdate();
            }

            // Restore cursor.
            Cursor.Current = cursor;
        }
        /// <summary>
        /// Gets the children text.
        /// </summary>
        /// <param name="compEntity">The comp entity.</param>
        /// <returns></returns>
        private string GetChildrenText(ICompositeEntity compEntity)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Entity entity in compEntity.ChildEntities)
            {
                sb.Append(GetText(entity));
            }

            return sb.ToString();
        }

        #endregion

        #region Switch Window
        #region Show Explorer
        private void label3_Click(object sender, EventArgs e)
        {
            showExplorer();
        }
        private void documentExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showExplorer();
        }

        private void showExplorer()
        {
            this.panel1.Visible = true;
            this.panel1.Dock = DockStyle.Fill;
            this.label3.BackColor = Color.FromArgb(235, 239, 245);
            this.panel1.BringToFront();
            this.panel2.Visible = false;
            this.panel2.Dock = DockStyle.None;
            this.label9.BackColor = Color.White;
            this.panel3.Visible = false;
            this.panel3.Dock = DockStyle.None;
            this.label11.BackColor = Color.White;
        }
        #endregion

        #region Show Viewer
        private void label9_Click(object sender, EventArgs e)
        {
            showViewer();
        }
        private void documentViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showViewer();
        }
        private void showViewer()
        {
            if (m_doc != null)
            {
                m_doc.Save(filepath);
                richTextBox1.Text = m_doc.GetText();
            }
            this.panel1.Visible = false;
            this.panel1.Dock = DockStyle.None;
            this.label3.BackColor = Color.White;
            this.panel2.Visible = true;
            this.panel2.BringToFront();
            this.panel2.Dock = DockStyle.Fill;
            this.label9.BackColor = Color.FromArgb(235, 239, 245);
            this.panel3.Visible = false;
            this.panel3.Dock = DockStyle.None;
            this.label11.BackColor = Color.White;
        }
        #endregion

        #region Show Log Window
        private void label11_Click(object sender, EventArgs e)
        {
            showLogWindow();
        }

        private void documentLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLogWindow();
        }
        private void showLogWindow()
        {
            this.panel1.Visible = false;
            this.panel1.Dock = DockStyle.None;
            this.label3.BackColor = Color.White;
            this.panel2.Visible = false;
            this.panel2.Dock = DockStyle.None;
            this.label9.BackColor = Color.White;
            this.panel3.Visible = true;
            this.panel3.BringToFront();
            this.panel3.Dock = DockStyle.Fill;
            this.label11.BackColor = Color.FromArgb(235, 239, 245);
        }
        #endregion

       

        #endregion
        #endregion
    }
}