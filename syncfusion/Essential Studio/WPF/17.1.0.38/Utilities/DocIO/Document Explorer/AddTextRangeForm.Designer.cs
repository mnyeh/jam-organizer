#region Copyright Syncfusion Inc. 2001-2019.
// Copyright Syncfusion Inc. 2001-2019. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
namespace Syncfusion.DocIOExplorer
{
  partial class AddTextRangeForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
      if( disposing && ( components != null ) )
      {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.txtText = new System.Windows.Forms.TextBox();
      this.btnAdd = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point( 12, 9 );
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size( 31, 13 );
      this.label1.TabIndex = 0;
      this.label1.Text = "Text:";
      // 
      // txtText
      // 
      this.txtText.Location = new System.Drawing.Point( 15, 25 );
      this.txtText.Multiline = true;
      this.txtText.Name = "txtText";
      this.txtText.Size = new System.Drawing.Size( 271, 52 );
      this.txtText.TabIndex = 1;
      // 
      // btnAdd
      // 
      this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnAdd.Location = new System.Drawing.Point( 211, 83 );
      this.btnAdd.Name = "btnAdd";
      this.btnAdd.Size = new System.Drawing.Size( 75, 23 );
      this.btnAdd.TabIndex = 2;
      this.btnAdd.Text = "Add";
      this.btnAdd.UseVisualStyleBackColor = true;
      // 
      // AddTextRangeForm
      // 
      this.AcceptButton = this.btnAdd;
      this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size( 302, 118 );
      this.Controls.Add( this.btnAdd );
      this.Controls.Add( this.txtText );
      this.Controls.Add( this.label1 );
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "AddTextRangeForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Add text range";
      this.ResumeLayout( false );
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtText;
    private System.Windows.Forms.Button btnAdd;
  }
}